import BaseApi from "./baseApi.ts";
import {AllMovies, MovieDetailed} from "../models/movie.ts";

const MOVIE_PREFIX = "/movies";

async function getSingleById(id: string) {
  return BaseApi.getSingleParams<MovieDetailed>(`${MOVIE_PREFIX}`, {params: {id: id}});
}

async function getAllByName(title: string, page: number) {
  return BaseApi.getSingleParams<AllMovies>(`${MOVIE_PREFIX}/search`, {
    params: {page: page, title: title}
  });
}

async function getTrending() {
  return BaseApi.getSingle<AllMovies>(`${MOVIE_PREFIX}/trending`);
}

const MovieApi = {
  getSingleById,
  getAllByName,
  getTrending,
};

export default MovieApi;
