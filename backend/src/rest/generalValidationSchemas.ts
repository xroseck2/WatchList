import { z } from 'zod';

export const queryPaginationRequestSchema = z.object({
  query: z.object({
    size: z.coerce.number().positive().optional(),
    page: z.coerce.number().positive().optional(),
  }),
});

export const paramsIdRequestSchema = z.object({
  params: z.object({
    id: z.string().cuid(),
  }),
});
