import {
  findUserByEmail, findUserById, findUserByName, getAllUsers, getPasswordHash,
} from './read';
import createUser from './create';
import { deleteUser } from './delete';
import {
  addPlaylistsToUser, deletePlaylistsFromUser, updateUser, userJoinClub, userLeaveClub,
} from './edit';

const userRepository = {
  findUserById,
  findUserByEmail,
  findUserByName,
  getAllUsers,
  createUser,
  deleteUser,
  updateUser,
  addPlaylistsToUser,
  deletePlaylistsFromUser,
  userJoinClub,
  userLeaveClub,
  getPasswordHash,
};

export default userRepository;
