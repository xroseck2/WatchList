import { Result } from '@badrap/result';
import prisma from '../../client';
import { PAGE_SIZE, Pagination } from '../../types';
import { NotFoundError } from '../../utils';
import { ReviewReadManyResult, ReviewReadResult } from './types/data';

export const findReviewById = async (id: string): ReviewReadResult => {
  try {
    const result = await prisma.review.findUnique({
      where: {
        id,
      },
      select: {
        id: true,
        rating: true,
        text: true,
        movieId: true,
        userId: true,
        createdAt: true,
        user: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such user'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findReviewByMovie = async (movieId: string): ReviewReadManyResult => {
  try {
    const result = await prisma.review.findMany({
      where: {
        movieId,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No reviews for this movie yet'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findReviewByUser = async (userId: string): ReviewReadManyResult => {
  try {
    const result = await prisma.review.findMany({
      where: {
        userId,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError(`No reviews by this user with id: ${userId}`));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const getAllReviews = async (pagination?: Pagination): ReviewReadManyResult => {
  const size = pagination?.size ?? PAGE_SIZE;
  const page = pagination?.page ?? 1;
  try {
    const result = await prisma.review.findMany({
      skip: (page - 1) * size,
      take: size,
    });
    if (!result) {
      return Result.err(new NotFoundError('No reviews found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};
