import React, {createContext, ReactNode, useState, useEffect} from 'react';
import {User} from "../models/user.ts";

type UserType = {
  id: string;
  name: string;
  email: string;
}

export type UserContextType = {
  user: UserType | null;
  setUser: React.Dispatch<React.SetStateAction<User | null>>;
};

type UserProviderProps = {
  children: ReactNode;
};

export const UserContext = createContext<UserContextType>(
    {
      user: null,
      setUser: () => {},
    });

export const UserProvider: React.FC<UserProviderProps> = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, []);

  useEffect(() => {
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
    } else {
      localStorage.removeItem('user');
    }
  }, [user]);

  return (
      <UserContext.Provider value={{user, setUser}}>
        {children}
      </UserContext.Provider>
  );
}
