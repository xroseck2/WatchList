import {Menu} from "antd";
import {PlusOutlined, UserOutlined, SearchOutlined, TeamOutlined, VideoCameraOutlined} from "@ant-design/icons";
import Sider from 'antd/es/layout/Sider';
import {useContext, useState} from "react";
import {useNavigate} from "react-router-dom";
import "./custom_sider.css"
import useWindowDimension from "../../hooks/useWindowDimension.tsx";
import {UserContext, UserContextType} from "../../contexts/UserContext.tsx";

function CustomSider() {
    const [collapsed, setCollapsed] = useState(false);
    const { width } = useWindowDimension();
    const navigate = useNavigate();
    const {user} = useContext<UserContextType>(UserContext);

    const itemsArray = [
        {   key: 'home',
            icon: <VideoCameraOutlined/>,
            label: 'Trending',
        },
        {   key: 'search',
            icon: <SearchOutlined/>,
            label: 'Search',
        },
        {   key: user ? "user/" + user.id : "/login",
            icon: <UserOutlined/>,
            label: user ? user.name : "Login/Register",
        }
    ];
    if (user) {
        itemsArray.push(
            {   key: 'add',
                icon: <PlusOutlined/>,
                label: 'Add review',
            },
            {   key: 'clubs',
                icon: <TeamOutlined/>,
                label: 'Your clubs',
            })
    }
    return (
        <Sider
            className="custom_sider"
            width="20%"
            collapsible={width > 700}
            collapsed={width > 700 ? collapsed : true}
            onCollapse={(value) => setCollapsed(value)}
        >
            <Menu className="sider__menu"
                theme="dark"
                mode="inline"
                defaultSelectedKeys={['home']}
                items={itemsArray}
                onClick={({key}) => navigate(key)}
            />
        </Sider>
    );
}

export default CustomSider;
