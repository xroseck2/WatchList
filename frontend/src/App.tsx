import './App.css'
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import routes from "./router/routes.tsx";
import { UserProvider } from "./contexts/UserContext.tsx";


const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <UserProvider>
        <RouterProvider router={createBrowserRouter(routes)} />
      </UserProvider>
    </QueryClientProvider>
  )
}

export default App
