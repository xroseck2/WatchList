import { Router } from 'express';
import { clubsController } from './controller';

/**
 * @swagger
 * tags:
 *   name: Clubs
 *   description: The clubs managing API
 * components:
 *   schemas:
 *     Club:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the club
 *         name:
 *           type: string
 *           description: The name of the club
 *         owner:
 *           $ref: '#/components/schemas/User'
 *           description: The owner of the club
 *         members:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/User'
 *           description: The members of the club
 *         playlists:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Playlist'
 *           description: The playlists in the club
 *         posts:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Post'
 *           description: The posts in the club
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The date the club was added
 *     CreateClubRequest:
 *       type: object
 *       required:
 *         - name
 *         - ownerId
 *       properties:
 *         name:
 *           type: string
 *           minLength: 1
 *           maxLength: 255
 *           description: The name of the club
 *         ownerId:
 *           type: string
 *           description: The ID of the owner
 *     UpdateClubRequest:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           minLength: 1
 *           maxLength: 255
 *           description: The new name of the club
 *         ownerId:
 *           type: string
 *           description: The new ID of the owner
 *     UpdateClubPlaylistsRequest:
 *       type: object
 *       required:
 *         - playlistIds
 *       properties:
 *         playlistIds:
 *           type: array
 *           items:
 *             type: string
 *             description: The ID of the playlist
 */
export const clubsRouter = Router();

/**
 * @swagger
 * /api/v1/clubs:
 *   get:
 *     summary: Return page with clubs
 *     tags: [Clubs]
 *     parameters:
 *       - in: query
 *         name: size
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The number of clubs to return
 *           required: false
 *       - in: query
 *         name: page
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The page number to return
 *           required: false
 *     responses:
 *       200:
 *         description: A list of clubs.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Club'
 *       500:
 *         description: Some server error
 */
clubsRouter.get('/', clubsController.getClubs);

/**
 * @swagger
 * /api/v1/clubs/search:
 *   get:
 *     summary: Search for a club by ID or name
 *     tags: [Clubs]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the club to search
 *         required: false
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *           description: The name of the club to search
 *         required: false
 *     responses:
 *       200:
 *         description: The club was successfully found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Club'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID or name did not match any existing club
 *       500:
 *         description: Some server error
 */
clubsRouter.get('/search', clubsController.getClub);

/**
 * @swagger
 * /api/v1/clubs:
 *   post:
 *     summary: Create a new club
 *     tags: [Clubs]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateClubRequest'
 *     responses:
 *       201:
 *         description: The club was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Club'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       500:
 *         description: Some server error
 */
clubsRouter.post('/', clubsController.createClub);

/**
 * @swagger
 * /api/v1/clubs/{id}:
 *   delete:
 *     summary: Delete a club
 *     tags: [Clubs]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the club to delete
 *           required: true
 *     responses:
 *       200:
 *         description: The club was successfully deleted
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Club'
 *       404:
 *         description: Bad request, the provided ID did not match any existing club
 *       500:
 *         description: Some server error
 */
clubsRouter.delete('/:id', clubsController.deleteClub);

/**
 * @swagger
 * /api/v1/clubs/{id}:
 *   put:
 *     summary: Update a club
 *     tags: [Clubs]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the club to update
 *           required: true
 *     requestBody:
 *       required: true
 *       content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UpdateClubRequest'
 *     responses:
 *       200:
 *         description: The club was successfully updated
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Club'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing club
 *       500:
 *         description: Some server error
 */
clubsRouter.put('/:id', clubsController.updateClub);

/**
 * @swagger
 * /api/v1/clubs/{id}/add_playlists:
 *   put:
 *     summary: Add playlists to a club
 *     tags: [Clubs]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the club to update
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateClubPlaylistsRequest'
 *     responses:
 *       200:
 *         description: The playlists were successfully added to the club
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Club'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing club
 *       500:
 *         description: Some server error
 */
clubsRouter.put('/:id/add_playlists', clubsController.addPlaylists);

/**
 * @swagger
 * /api/v1/clubs/{id}/remove_playlists:
 *   put:
 *     summary: Remove playlists from a club
 *     tags: [Clubs]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the club to update
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateClubPlaylistsRequest'
 *     responses:
 *       200:
 *         description: The playlists were successfully removed from the club
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Club'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing club
 *       500:
 *         description: Some server error
 */
clubsRouter.put('/:id/remove_playlists', clubsController.removePlaylists);
