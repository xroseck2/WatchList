import {Button, Flex, Form, FormProps, Input, Modal} from "antd";
import './login_page.css';
import {UserContext, UserContextType} from '../../contexts/UserContext.tsx';
import {useContext} from "react";
import LogoutPanel from "../../components/LogoutPanel/LogoutPanel.tsx";
import {useNavigate} from "react-router-dom";
import userApi from "../../api/userApi.ts";

type FieldType = {
  email?: string;
  password?: string;
};

export default function LoginPage() {
  const {user, setUser} = useContext<UserContextType>(UserContext);
  const navigate = useNavigate();

    const verifyUser = async (email: string, password: string): Promise<boolean> => {
      return userApi.verifyUser(email, password)
        .then((res) => {
          if (res.status === 200) {
            return userApi.getSingleByEmail(email)
              .then(function (resIn){
                const loggedUser = {
                  id: resIn.id,
                  name: resIn.name,
                  email: resIn.email,
                  createdAt: resIn.createdAt
                }
                setUser(loggedUser)
                return true;
              })
              .catch(function (err){
                console.log(err);
                return false;
              })
          }
          return false;
        })
        .catch(function(err) {
          console.log(err);
          return false;
        })
    }
  const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
    verifyUser(values.email!, values.password!).then( (v) => {
      if (v) {
        navigate("/home");
        Modal.success({
          title: 'Login successfully',
          content: "User logged in successfully!",
        });
      } else {
        Modal.error({
          title: 'Login failed',
          content: "User was not logged in!",
        });
      }
      }
    )
  };

  const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
      <div>
        {
          user ? (
              <LogoutPanel/>
          ) : (
              <Form
                  labelCol={{ span: 4 }}
                  wrapperCol={{ span: 20 }}
                  className="login__box"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
              >
                <Form.Item<FieldType>
                    label="Email"
                    name="email"
                    rules={[{ required: true, type: "email", message: 'Email is in wrong format!' }]}
                >
                  <Input />
                </Form.Item>

                <Form.Item<FieldType>
                    label="Password"
                    name="password"
                    rules={[{ min: 10, required: true, message: 'Please input your password!' }]}
                >
                  <Input.Password />
                </Form.Item>
                <Button onClick={() => navigate("/register")}
                    type="text">
                  Don't have an account? Sign up
                </Button>
                <Flex justify="center">
                  <Form.Item>
                    <Button type="primary" htmlType="submit">
                      Submit
                    </Button>
                  </Form.Item>
                </Flex>
              </Form>
          )
        }
      </div>
  );
}
