import { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import { SALT_ROUNDS } from '../../types';
import { handleRepositoryErrors, parseRequest } from '../../utils';
import repository from '../../repositories/users/index';
import {
  queryPaginationRequestSchema,
  paramsIdRequestSchema,
} from '../generalValidationSchemas';
import {
  createUserRequestSchema,
  updateUserRequestSchema,
  searchUserRequestSchema,
  updateUserPlaylistsRequestSchema,
  updateUserClubsRequestSchema,
  verifyUserRequestSchema,
} from './validationSchemas';

const getUsers = async (req: Request, res: Response) => {
  const request = await parseRequest(queryPaginationRequestSchema, req, res);
  if (request === null) return;

  const result = await repository.getAllUsers(request.query);
  if (result.isErr) {
    handleRepositoryErrors(result.error, res);
    return;
  }

  res.send(result.value);
};

const createUser = async (req: Request, res: Response) => {
  const request = await parseRequest(createUserRequestSchema, req, res);
  if (request === null) return;

  const hashedPassword = await bcrypt.hash(request.body.password, SALT_ROUNDS);

  const userWithHashedPassword = {
    ...request.body,
    password: hashedPassword,
  };

  const createdUser = await repository.createUser(userWithHashedPassword);
  if (createdUser.isErr) {
    handleRepositoryErrors(createdUser.error, res);
    return;
  }

  res.status(201).send(createdUser.value);
};

const deleteUser = async (req: Request, res: Response) => {
  const request = await parseRequest(paramsIdRequestSchema, req, res);
  if (request === null) return;

  const deletedUser = await repository.deleteUser({ id: request.params.id });
  if (deletedUser.isErr) {
    handleRepositoryErrors(deletedUser.error, res);
    return;
  }

  res.send(deletedUser.value);
};

const updateUser = async (req: Request, res: Response) => {
  const request = await parseRequest(updateUserRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedUser = await repository.updateUser(updateData);
  if (updatedUser.isErr) {
    handleRepositoryErrors(updatedUser.error, res);
    return;
  }

  res.send(updatedUser.value);
};

const getUser = async (req: Request, res: Response) => {
  const request = await parseRequest(searchUserRequestSchema, req, res);
  if (request === null) return;

  const { id, name, email } = request.query;

  let user;
  if (id !== undefined) {
    user = await repository.findUserById(id);
  } else if (name !== undefined) {
    user = await repository.findUserByName(name);
  } else if (email !== undefined) {
    user = await repository.findUserByEmail(email);
  }

  if (user?.isErr) {
    handleRepositoryErrors(user.error, res);
    return;
  }

  res.send(user?.value);
};

const addPlaylists = async (req: Request, res: Response) => {
  const request = await parseRequest(updateUserPlaylistsRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedUser = await repository.addPlaylistsToUser(updateData);
  if (updatedUser.isErr) {
    handleRepositoryErrors(updatedUser.error, res);
    return;
  }

  res.send(updatedUser.value);
};

const removePlaylists = async (req: Request, res: Response) => {
  const request = await parseRequest(updateUserPlaylistsRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedUser = await repository.deletePlaylistsFromUser(updateData);
  if (updatedUser.isErr) {
    handleRepositoryErrors(updatedUser.error, res);
    return;
  }

  res.send(updatedUser.value);
};

const joinClub = async (req: Request, res: Response) => {
  const request = await parseRequest(updateUserClubsRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedUser = await repository.userJoinClub(updateData);
  if (updatedUser.isErr) {
    handleRepositoryErrors(updatedUser.error, res);
    return;
  }

  res.send(updatedUser.value);
};

const leaveClub = async (req: Request, res: Response) => {
  const request = await parseRequest(updateUserClubsRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedUser = await repository.userLeaveClub(updateData);
  if (updatedUser.isErr) {
    handleRepositoryErrors(updatedUser.error, res);
    return;
  }

  res.send(updatedUser.value);
};

const verifyUser = async (req: Request, res: Response) => {
  const request = await parseRequest(verifyUserRequestSchema, req, res);
  if (request === null) return;

  const passwordHash = await repository.getPasswordHash(request.body);
  if (passwordHash.isErr) {
    handleRepositoryErrors(passwordHash.error, res);
    return;
  }

  const passwordMatch = await bcrypt.compare(request.body.password, passwordHash.value.password);
  if (!passwordMatch) {
    res.status(401).send({ message: 'Invalid credentials' });
    return;
  }

  res.send({ message: 'User verified successfully' });
};

export const usersController = {
  getUsers,
  createUser,
  deleteUser,
  updateUser,
  getUser,
  addPlaylists,
  removePlaylists,
  joinClub,
  leaveClub,
  verifyUser,
};
