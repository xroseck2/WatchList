import type { Result } from '@badrap/result';

export const PAGE_SIZE = 10;
export const SALT_ROUNDS = 10;
export const SEED_COUNT = 20;

export type DbResult<T> = Promise<Result<T>>;

export type Pagination = {
  size?: number | undefined
  page?: number | undefined
};
