import {Flex} from "antd";
import Trending from "../../components/Trending/Trending.tsx";


export default function HomePage() {

  return (
    <Flex vertical align="center" >
        <Trending/>
    </Flex>
  );
}