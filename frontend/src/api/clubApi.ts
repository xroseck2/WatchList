import BaseApi from "./baseApi";
import { ReqPagination } from "../models/request";
import { Club, ClubPlaylists, CreateClub } from "../models/club.ts";

const CLUB_PREFIX = "/clubs";

async function getSingleById(id: string) {
  return BaseApi.getSingleParams<Club>(`${CLUB_PREFIX}/search`, {params: {id: id}});
}

async function getAllByName(name: string) {
  return BaseApi.getAll<Club>(`${CLUB_PREFIX}/search`, {params: {name: name}});
}

async function getAllPaginated({ page }: ReqPagination) {
  return BaseApi.getAllPaginated<Club>(CLUB_PREFIX, {
    params: { page },
  });
}

async function createSingle(payload: CreateClub) {
  return BaseApi.postSingle<Club>(CLUB_PREFIX, payload);
}

async function updateSingle(id: string, payload: CreateClub) {
  return BaseApi.putSingle<Club>(`${CLUB_PREFIX}/${id}`, payload);
}

async function updateAddPlaylist(id: string, payload: ClubPlaylists) {
  return BaseApi.putSingle<Club>(`${CLUB_PREFIX}/${id}/add_playlists`, payload);
}

async function updateRemovePlaylist(id: string, payload: ClubPlaylists) {
  return BaseApi.putSingle<Club>(`${CLUB_PREFIX}/${id}/remove_playlists`, payload);
}

async function deleteSingle(id: string) {
  return BaseApi.deleteSingle<Club>(`${CLUB_PREFIX}/${id}`);
}

const CategoriesApi = {
  getSingleById,
  getAllByName,
  getAllPaginated,
  createSingle,
  updateSingle,
  updateAddPlaylist,
  updateRemovePlaylist,
  deleteSingle,
};

export default CategoriesApi;
