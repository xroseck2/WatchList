import { Playlist } from '@prisma/client';
import { DbResult } from '../../../types';

export type PlaylistReadManyResult = DbResult<Playlist[]>;
export type PlaylistReadResult = DbResult<Playlist>;

export type PlaylistCreateData = {
  name: string,
};

export type PlaylistCreateWithMoviesData = PlaylistCreateData & {
  movieIds: string[]
};

export type PlaylistIdData = {
  id: string
};

export type PlaylistEditData = PlaylistIdData & {
  name?: string | undefined
};

export type PlaylistMoviesData = PlaylistIdData & {
  movieIds: string[]
};
