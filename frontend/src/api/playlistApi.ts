import BaseApi from "./baseApi";
import { ReqPagination } from "../models/request";
import {CreatePlaylist, Playlist, PlaylistMovie, UpdatePlaylist} from "../models/playlist.ts";

const PLAYLIST_PREFIX = "/playlists";

async function getSingleById(id: string) {
  return BaseApi.getSingleParams<Playlist>(`${PLAYLIST_PREFIX}/search`, {params: {id: id}});
}

async function getSingleByName(name: string) {
  return BaseApi.getSingleParams<Playlist>(`${PLAYLIST_PREFIX}/search`, {params: { name: name}});
}

// TODO finding playlists by clubId or userId

async function getAllPaginated({ page }: ReqPagination) {
  return BaseApi.getAllPaginated<Playlist>(PLAYLIST_PREFIX, {
    params: { page },
  });
}

async function createSingle(payload: CreatePlaylist) {
  return BaseApi.postSingle<Playlist>(PLAYLIST_PREFIX, payload);
}

async function updateSingle(id: string, payload: UpdatePlaylist) {
  return BaseApi.putSingle<Playlist>(`${PLAYLIST_PREFIX}/${id}`, payload);
}

async function updateAddMovies(id: string, payload: PlaylistMovie) {
  return BaseApi.putSingle<Playlist>(`${PLAYLIST_PREFIX}/${id}/add_movies`, payload);
}

async function updateRemoveMovies(id: string, payload: PlaylistMovie) {
  return BaseApi.putSingle<Playlist>(`${PLAYLIST_PREFIX}/${id}/remove_movies`, payload);
}

async function deleteSingle(id: string) {
  return BaseApi.deleteSingle<Playlist>(`${PLAYLIST_PREFIX}/${id}`);
}

const PlaylistApi = {
  getSingleById,
  getSingleByName,
  getAllPaginated,
  createSingle,
  updateSingle,
  updateAddMovies,
  updateRemoveMovies,
  deleteSingle,
};

export default PlaylistApi;
