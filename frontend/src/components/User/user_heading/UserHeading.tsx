import {Avatar, Button, Flex, Typography} from "antd";
import "./user_heading.css"
import {useContext} from "react";
import {UserContext, type UserContextType} from "../../../contexts/UserContext.tsx";
import {useNavigate} from "react-router-dom";

const {Text} = Typography;

interface HeadingProps {
    username: string;
    id: string;
    createdDate: string;
}

function UserHeading({username, createdDate, id}: HeadingProps) {
    const {user, setUser} = useContext<UserContextType>(UserContext);
    const navigate = useNavigate();
    return (
        <Flex className="box" justify="space-between" align="end">
            <Flex >
                <Avatar className="user_heading__avatar" shape="square" size={100} src="/default_user.jpg"/>
                <Flex vertical className="user_heading__details">
                    <Text className="details__name">
                        {username}
                    </Text>
                    <Text italic>
                        User joined in {createdDate.slice(0,4)}
                    </Text>
                </Flex>
            </Flex>
            {user?.id == id &&
                <Button onClick={() => {
                    setUser(null);
                    navigate("/login");
                }}>{user ? "LOGOUT" : "LOGIN"}</Button>
            }

        </Flex>
    );
}

export default UserHeading;