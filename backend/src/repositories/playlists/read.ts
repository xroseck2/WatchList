import { Result } from '@badrap/result';
import prisma from '../../client';
import { PAGE_SIZE, Pagination } from '../../types';
import { NotFoundError } from '../../utils';
import { PlaylistReadManyResult, PlaylistReadResult } from './types/data';

export const findPlaylistById = async (id: string): PlaylistReadResult => {
  try {
    const result = await prisma.playlist.findUnique({
      where: {
        id,
      },
      select: {
        id: true,
        name: true,
        moviesIds: true,
        createdAt: true,
        clubs: true,
        user: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such playlist'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findPlaylistByName = async (name: string): PlaylistReadManyResult => {
  try {
    const result = await prisma.playlist.findMany({
      where: {
        name: {
          contains: name,
        },
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such playlist'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findPlaylistByUser = async (userId: string): PlaylistReadManyResult => {
  try {
    const result = await prisma.playlist.findMany({
      where: {
        user: {
          some: {
            id: userId,
          },
        },
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such playlist'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findPlaylistByClub = async (clubId: string): PlaylistReadManyResult => {
  try {
    const result = await prisma.playlist.findMany({
      where: {
        clubs: {
          some: {
            id: clubId,
          },
        },
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such playlist'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const getAllPlaylists = async (pagination?: Pagination): PlaylistReadManyResult => {
  const size = pagination?.size ?? PAGE_SIZE;
  const page = pagination?.page ?? 1;
  try {
    const result = await prisma.playlist.findMany({
      skip: (page - 1) * size,
      take: size,
    });
    if (!result) {
      return Result.err(new NotFoundError('No playlists found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};
