import {BaseModelId, CreatedAtModel} from "./base.ts";
import {Review} from "./review.ts";
import {Post} from "./post.ts";
import {Playlist} from "./playlist.ts";
import {Club} from "./club.ts";

export type User = (BaseModelId & CreatedAtModel) & {
  name: string;
  email: string;
};

export type CreateUser = {
  name: string;
  email: string;
  password: string;
};

export type UserPlaylist = {
  playlistIds: string[];
}

export type UserClub = {
  clubId: string;
}

export type UserExtended = User & {
  ownedClubs: Club[];
  clubs: Club[];
  reviews: Review[];
  playlists: Playlist[];
  posts: Post[];
};

