import {BaseModelId, CreatedAtModel} from "./base.ts";
import {Movie} from "./movie.ts";
import {Club} from "./club.ts";
import {User} from "./user.ts";

export type Playlist = (BaseModelId & CreatedAtModel) & {
  name: string;
  movies: Movie[];
  club: Club;
  owner: User;
}

export type CreatePlaylist = {
  name: string;
  movieIds: string[];
}

export type UpdatePlaylist = {
  name: string;
}

export type PlaylistMovie = {
  movieIds: string[]
}