import {Flex, Typography} from "antd";
import "./review_tile.css";
import {Link} from "react-router-dom";
import {useEffect, useState} from "react";
import {Review} from "../../../models/review.ts";
import reviewApi from "../../../api/reviewApi.ts";
import {MovieDetailed} from "../../../models/movie.ts";
import movieApi from "../../../api/movieApi.ts";
import userApi from "../../../api/userApi.ts";
import {UserExtended} from "../../../models/user.ts";
import {formatCreatedAt} from "../../../utils.ts";
const {Text, Paragraph} = Typography;

interface ReviewProps {
    review: Review;
    userId: string;
}

function ReviewTile({review, userId}: ReviewProps) {
    const [ellipsis] = useState(true);
    const [rev, setRev] = useState(review);
    const [user, setUser] = useState<UserExtended | null>(null);
    const [movie, setMovie] = useState<MovieDetailed | null>(null);
    useEffect(() => {
        async function fetchReviewAndMovie() {
            try {
                console.log("fetching")
                const fetchedReview = await reviewApi.getSingleById(review.id);
                setRev(fetchedReview);
                if (fetchedReview.movieId) {
                    const fetchedMovie = await movieApi.getSingleById(fetchedReview.movieId);
                    setMovie(fetchedMovie);
                }
            } catch (error) {
                console.error('Error fetching review or movie:', error);
            }
        }
        fetchReviewAndMovie();
    }, [review.id]);

    useEffect(() => {
        userApi.getSingleById(userId).then((value) => {
            setUser(value);
        })
    }, [userId]);


    return (
        <Flex vertical className="box">
            <Flex justify="space-between" align="center">
                <Flex>
                    <Link to={`/movie/${movie?.id}`}>
                        <Text className="review_tile__heading clickable" strong>{movie?.title || 'Loading...'}</Text>
                    </Link>
                    <Text className="review_tile__heading">({movie?.release_date?.slice(0, 4) || 'N/A'})</Text>
                </Flex>
                <Text className="user__club_text review_tile__rating" strong>{rev.rating}/10</Text>
            </Flex>
            <Link to={"/user/" + userId}>
                {
                    user?.name &&
                    <Flex justify="space-between" align="center">
                        <Text className="review_tile__username" strong>
                            {user?.name}
                        </Text>
                        <Text>
                            {formatCreatedAt(rev.createdAt)}
                        </Text>
                    </Flex>

                }
            </Link>
            <Flex className="review_tile__review">
                <Paragraph ellipsis={ellipsis ? { rows: 3, expandable: true, symbol: 'more' } : false}>
                    {rev.text}
                </Paragraph>

            </Flex>
        </Flex>
    );
}

export default ReviewTile;