import { deleteClub } from './delete';
import { createClub } from './create';
import { addPlaylistsToClub, deletePlaylistsFromClub, updateClub } from './edit';
import { findClubById, findClubByName, getAllClubs } from './read';

const clubRepository = {
  createClub,
  deleteClub,
  updateClub,
  findClubById,
  findClubByName,
  getAllClubs,
  addPlaylistsToClub,
  deletePlaylistsFromClub,
};

export default clubRepository;
