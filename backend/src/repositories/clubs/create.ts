import { Result } from '@badrap/result';
import { Prisma } from '@prisma/client';
import prisma from '../../client';
import { ClubCreateData, ClubReadResult } from './types/data';
import { ConflictError } from '../../utils';

export const createClub = async (data: ClubCreateData): ClubReadResult => {
  try {
    const initialPlaylists: Prisma.PlaylistCreateWithoutUserInput[] = [
      {
        name: 'Club\'s playlist',
      },
    ];
    const newClub = await prisma.club.create({
      data: {
        name: data.name,
        owner: {
          connect: {
            id: data.ownerId,
          },
        },
        playlists: {
          create: initialPlaylists,
        },
      },
    });
    return Result.ok(newClub);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      if (error.code === 'P2002') {
        return Result.err(new ConflictError(`Club with name: ${data.name} already exists`));
      }
    }
    return Result.err(error as Error);
  }
};
