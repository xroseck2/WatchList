import { deleteReview } from './delete';
import createReview from './create';
import { updateReview } from './edit';
import {
  findReviewById, findReviewByMovie, findReviewByUser, getAllReviews,
} from './read';

const reviewRepository = {
  createReview,
  deleteReview,
  updateReview,
  findReviewById,
  findReviewByUser,
  findReviewByMovie,
  getAllReviews,

};

export default reviewRepository;
