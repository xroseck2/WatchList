import { Result } from '@badrap/result';
import prisma from '../../client';
import { PlaylistCreateData, PlaylistCreateWithMoviesData, PlaylistReadResult } from './types/data';

export const createPlaylist = async (data: PlaylistCreateData): PlaylistReadResult => {
  try {
    const newPlaylist = await prisma.playlist.create({
      data: {
        name: data.name,
      },
    });
    return Result.ok(newPlaylist);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const createPlaylistWithMovies = async (data: PlaylistCreateWithMoviesData): PlaylistReadResult => {
  try {
    const newPlaylist = await prisma.playlist.create({
      data: {
        name: data.name,
        moviesIds: data.movieIds,
      },
    });
    return Result.ok(newPlaylist);
  } catch (error) {
    return Result.err(error as Error);
  }
};
