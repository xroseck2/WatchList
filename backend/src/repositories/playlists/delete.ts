import { Result } from '@badrap/result';
import prisma from '../../client';
import { NotFoundError } from '../../utils';
import { PlaylistIdData, PlaylistReadManyResult, PlaylistReadResult } from './types/data';

export const deletePlaylist = async (data: PlaylistIdData): PlaylistReadResult => {
  try {
    const deleted = await prisma.playlist.delete({
      where: {
        id: data.id,
      },
    });
    if (!deleted) {
      return Result.err(new NotFoundError('No playlist found'));
    }
    return Result.ok(deleted);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const deletePlaylistsWithoutOwners = async (): PlaylistReadManyResult => {
  try {
    const emptyPlaylists = await prisma.playlist.findMany({
      where: {
        AND: [
          { user: { none: {} } },
          { clubs: { none: {} } },
        ],
      },
    });

    const deletedPlaylists = await prisma.$transaction(
      emptyPlaylists.map((playlist) => prisma.playlist.delete({
        where: { id: playlist.id },
      })),
    );

    return Result.ok(deletedPlaylists);
  } catch (error) {
    return Result.err(error as Error);
  }
};
