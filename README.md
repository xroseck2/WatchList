
# MovieMates
## Students:

- Patrik Rosecký, učo 525015, FI B-PVA PVA [sem 6, roč 3]
- Jan Poláček, učo 536681, FI B-PVA PVA [sem 4, roč 2]
- Radim Rychlík, učo 514233, FI B-PVA PVA [sem 4, roč 2]

## Oficiální zadání:

Create a social network based on movies. Users create their watchlists and add movies and their rating. The users can see their friends' recently watched movies. Each user can see their statistics - what genres they watch the most, how many movies they have watched, the average rating of their movie, etc. The users can create their own movie clubs (they can be just a "group" of friends or based on movie genres and interests). The club members can recommend movies to each other. The users can create a new movie "record" if the movie is not in the system yet. Before this record gets into the system, it has to be reviewed by the site administrator. The administrator can approve or deny the movie, or they might request changes to the record (for example when the information about the movie is incorrect). The movies on the site can be filtered, full-text searched, and rated. The ratings for each movie are available on the movie's specific page detail.