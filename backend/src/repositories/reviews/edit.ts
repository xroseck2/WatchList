import { Prisma } from '@prisma/client';
import { Result } from '@badrap/result';
import prisma from '../../client';
import { NotFoundError } from '../../utils';
import { ReviewEditData, ReviewReadResult } from './types/data';

export const updateReview = async (data: ReviewEditData): ReviewReadResult => {
  const updateData: Prisma.ReviewUpdateInput = {
    ...(data.text && { text: data.text }),
    ...(data.rating && { rating: data.rating }),
  };

  try {
    const result = await prisma.review.update({
      where: {
        id: data.id,
      },
      data: updateData,
    });
    if (!result) {
      return Result.err(new NotFoundError('No users found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};
