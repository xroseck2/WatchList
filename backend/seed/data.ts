import { Prisma } from '@prisma/client';
import { faker } from '@faker-js/faker';
import { env } from 'process';

const axios = require('axios');
const cuid = require('cuid');

const getTrendingMoviesIds = async (): Promise<string[]> => {
  const trendingMovies = await axios.get('https://api.themoviedb.org/3/trending/movie/day', {
    headers: {
      Authorization: `Bearer ${env.MOVIE_DATABASE_TOKEN}`,
      accept: 'application/json',
    },
    params: {
      language: 'en-US',
    },
  });

  return trendingMovies.data.results.map((movie: { id: number }) => movie.id.toString());
};

const seedData = {
  users: [] as Prisma.UserCreateManyInput[],
  clubs: [] as Prisma.ClubCreateManyInput[],
  reviews: [] as Prisma.ReviewCreateManyInput[],
  playlists: [] as Prisma.PlaylistCreateManyInput[],
  posts: [] as Prisma.PostCreateManyInput[],
};

const fillSeedData = async (count: number): Promise<typeof seedData> => {
  const trendingMoviesIds = await getTrendingMoviesIds();

  for (let i = 0; i < count; i += 1) {
    seedData.users.push({
      id: cuid(),
      email: faker.internet.email(),
      name: faker.internet.userName(),
      password: faker.internet.password({ length: 10 }),
    });
    seedData.clubs.push({
      id: cuid(),
      name: faker.word.noun(),
      ownerId: seedData.users[i]!.id!,
    });
    trendingMoviesIds.forEach((movieId) => {
      seedData.reviews.push({
        rating: faker.number.float({ multipleOf: 0.5, min: 0, max: 10 }),
        text: faker.lorem.lines({ min: 2, max: 2 }),
        movieId,
        userId: seedData.users[i]!.id!,
      });
    });
    seedData.playlists.push({
      id: cuid(),
      name: faker.word.noun(),
      moviesIds: trendingMoviesIds,
    });
    seedData.posts.push(
      {
        message: faker.lorem.lines({ min: 3, max: 3 }),
        authorId: seedData.users[i]!.id!,
        clubId: seedData.clubs[i]!.id!,
      },
      {
        message: faker.lorem.lines({ min: 3, max: 3 }),
        authorId: seedData.users[i]!.id!,
        clubId: seedData.clubs[i]!.id!,
      },
    );
  }

  return seedData;
};

export default fillSeedData;
