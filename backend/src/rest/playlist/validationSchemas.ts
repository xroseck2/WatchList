import { z } from 'zod';
import { paramsIdRequestSchema } from '../generalValidationSchemas';

export const createPlaylistRequestSchema = z.object({
  body: z.object({
    name: z.string().min(1).max(255),
    movieIds: z.array(z.string()).optional(),
  }),
});

export const updatePlaylistRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      name: z.string().min(1).max(255).optional(),
    }),
  }),
);

export const updatePlaylistMoviesRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      movieIds: z.array(z.string()),
    }),
  }),
);

export const searchPlaylistRequestSchema = z.object({
  query: z.object({
    id: z.string().cuid().optional(),
    name: z.string().min(1).max(255).optional(),
    userId: z.string().cuid().optional(),
    clubId: z.string().cuid().optional(),
  }),
}).refine((data) => {
  const definedProps = Object.values(data.query).filter((prop) => prop !== undefined);
  return definedProps.length >= 1;
}, {
  message: "Either 'id', 'name', 'userId' or 'clubId' must be provided",
});
