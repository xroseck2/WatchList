import { Result } from '@badrap/result';
import prisma from '../../client';
import { PAGE_SIZE, Pagination } from '../../types';
import { NotFoundError } from '../../utils';
import { ClubReadManyResult, ClubReadResult } from './types/data';

export const findClubById = async (id: string): ClubReadResult => {
  try {
    const result = await prisma.club.findUnique({
      where: {
        id,
      },
      select: {
        id: true,
        name: true,
        ownerId: true,
        createdAt: true,
        owner: true,
        members: true,
        playlists: true,
        post: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such club'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findClubByName = async (name: string): ClubReadManyResult => {
  try {
    const result = await prisma.club.findMany({
      where: {
        name: {
          contains: name,
        },
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such club'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const getAllClubs = async (pagination?: Pagination): ClubReadManyResult => {
  const size = pagination?.size ?? PAGE_SIZE;
  const page = pagination?.page ?? 1;
  try {
    const result = await prisma.club.findMany({
      skip: (page - 1) * size,
      take: size,
    });
    if (!result) {
      return Result.err(new NotFoundError('No clubs found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};
