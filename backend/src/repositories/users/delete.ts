import { Result } from '@badrap/result';
import { UserIdData, UserReadResult } from './types/data';
import prisma from '../../client';
import { NotFoundError } from '../../utils';
import { deletePlaylistsWithoutOwners } from '../playlists/delete';

export const deleteUser = async (data: UserIdData): UserReadResult => {
  try {
    const deleted = await prisma.user.delete({
      where: {
        id: data.id,
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
      },
    });

    if (!deleted) {
      return Result.err(new NotFoundError('No users found'));
    }

    await deletePlaylistsWithoutOwners();

    return Result.ok(deleted);
  } catch (error) {
    return Result.err(error as Error);
  }
};
