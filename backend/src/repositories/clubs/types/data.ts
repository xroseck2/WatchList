import { Club } from '@prisma/client';
import { DbResult } from '../../../types';

export type ClubReadManyResult = DbResult<Club[]>;
export type ClubReadResult = DbResult<Club>;

export type ClubCreateData = {
  name: string,
  ownerId: string
};

export type ClubIdData = {
  id: string
};

export type ClubEditData = ClubIdData & {
  name?: string | undefined
  ownerId?: string | undefined
};

export type ClubPlaylistsData = ClubIdData & {
  playlistIds: string[]
};
