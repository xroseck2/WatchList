import { Request, Response } from 'express';
import { handleRepositoryErrors, parseRequest } from '../../utils';
import repository from '../../repositories/posts/index';
import {
  queryPaginationRequestSchema,
} from '../generalValidationSchemas';
import {
  createPostRequestSchema,
  searchPostRequestSchema,
} from './validationSchemas';

const getPosts = async (req: Request, res: Response) => {
  const request = await parseRequest(queryPaginationRequestSchema, req, res);
  if (request === null) return;

  const result = await repository.getAllPosts(request.query);
  if (result.isErr) {
    handleRepositoryErrors(result.error, res);
    return;
  }

  res.send(result.value);
};

const createPost = async (req: Request, res: Response) => {
  const request = await parseRequest(createPostRequestSchema, req, res);
  if (request === null) return;

  const createdPost = await repository.createPost(request.body);
  if (createdPost.isErr) {
    handleRepositoryErrors(createdPost.error, res);
    return;
  }

  res.status(201).send(createdPost.value);
};

const getPost = async (req: Request, res: Response) => {
  const request = await parseRequest(searchPostRequestSchema, req, res);
  if (request === null) return;

  const { id, authorId, clubId } = request.query;

  const pagination = {
    size: request.query.size,
    page: request.query.page,
  };

  let post;
  if (id !== undefined) {
    post = await repository.findPostById(id);
  } else if (authorId !== undefined) {
    post = await repository.findPostByAuthorId(authorId, pagination);
  } else if (clubId !== undefined) {
    post = await repository.findPostByClubId(clubId, pagination);
  }

  if (post?.isErr) {
    handleRepositoryErrors(post.error, res);
    return;
  }

  res.send(post?.value);
};

export const postsController = {
  getPosts,
  createPost,
  getPost,
};
