import { Result } from '@badrap/result';
import prisma from '../../client';
import { NotFoundError } from '../../utils';
import { ClubIdData, ClubReadResult } from './types/data';
import { deletePlaylistsWithoutOwners } from '../playlists/delete';

export const deleteClub = async (data: ClubIdData): ClubReadResult => {
  try {
    const deleted = await prisma.club.delete({
      where: {
        id: data.id,
      },
    });

    if (!deleted) {
      return Result.err(new NotFoundError('No clubs found'));
    }

    await deletePlaylistsWithoutOwners();

    return Result.ok(deleted);
  } catch (error) {
    return Result.err(error as Error);
  }
};
