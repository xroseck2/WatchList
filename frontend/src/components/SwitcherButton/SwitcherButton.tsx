import {Button} from "antd";
import "./switcher_button.css"
interface SwitcherButtonProps {
    title: string;
    setIndex: () => void;
    selected: boolean;
}

function SwitcherButton({title, setIndex, selected}: SwitcherButtonProps) {
    return (
        <Button className={`switcher_button`} type={selected ? "primary" : "text"}
                onClick={setIndex} size="large"
        >
            {title}
        </Button>
    );
}

export default SwitcherButton;