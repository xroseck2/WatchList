import {Link, useParams} from "react-router-dom";
import {useContext, useEffect, useState} from "react";
import {Playlist} from "../../models/playlist.ts";
import playlistApi from "../../api/playlistApi.ts";
import {Flex, Typography} from "antd";
import MovieCard from "../../components/MovieCard/MovieCard.tsx";
import {MovieDetailed} from "../../models/movie.ts";
import movieApi from "../../api/movieApi.ts";
import {UserContext, UserContextType} from "../../contexts/UserContext.tsx";
const {Text, Title} = Typography;

function PlaylistPage() {
    const {playlistId} = useParams();
    const {user} = useContext<UserContextType>(UserContext);
    const [playlist, setPlaylist] = useState<Playlist | null>(null);
    const [movies, setMovies] = useState<MovieDetailed[]>([]);

    useEffect(() => {
        playlistApi.getSingleById(playlistId ?? "error").then(value => {
            console.log(value);
            setPlaylist(value);
        });
    }, [playlistId])
    // console.log(playlist.user[0].name);
    useEffect(() => {
        Promise.all(playlist?.moviesIds.map((id: string) => movieApi.getSingleById(id)))
            .then((movies: MovieDetailed[]) => {
                setMovies(movies);
            })
            .catch((error) => {
                console.error(error);
            });
    }, [playlist?.moviesIds]);
    if (!playlist) {
        return <Text>Not found</Text>
    }

    const remove = () => {
        playlistApi.updateRemoveMovies(playlist.id, {movieIds: [String(movie.id)]})
            .then((val) => {
                    setPlaylist(val)
                }
            )
    }
    console.log(playlist)
    return (
        <Flex vertical align="center">
            <Title>Playlist: {playlist.name}</Title>
            <Link to={"/user/" + playlist.user[0].id}>
                <Text className="clickable" style={{fontSize: "x-large"}}>Owner: {playlist.user[0].name}</Text>
            </Link>
            {movies?.map((movie) => (
                <MovieCard movie={movie} removeFromPlaylist={playlist.user[0].id == user.id ? remove : null} />
            ))}
        </Flex>
    );
}

export default PlaylistPage;