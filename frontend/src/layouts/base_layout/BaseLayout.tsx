import {Outlet} from "react-router-dom";
import {Image, Layout, Row, Typography} from "antd";
import CustomSider from "../../components/CustomSider/CustomSider.tsx";
import "./base_layout.css"
const Text = Typography
const { Header, Footer, Content } = Layout;

function BaseLayout() {
    return (
        <Layout className="base_layout">
            <CustomSider/>
            <Layout>
                <Header className="base_layout__header" >
                    <Row justify="center">
                            <Image src="/big_no_text.png" height={36} preview={false}/>
                            <Text className="header-text"> MovieMates</Text>
                    </Row>
                </Header>
                <Content className="primary_color">
                    <Outlet/>
                </Content>
                <Footer className="base_layout__footer">@2024 RaJaPa corporation</Footer>
            </Layout>
        </Layout>
    );
}

export default BaseLayout;