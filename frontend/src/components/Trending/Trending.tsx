import {useEffect,useState} from "react";
import {Flex} from "antd";
import movieApi from "../../api/movieApi.ts";
import MovieCard from "../MovieCard/MovieCard.tsx";
import { MovieDetailed} from "../../models/movie.ts";

const Trending: React.FC = () => {
  const [movies, setMovies] = useState<MovieDetailed[]>([]);

  useEffect(() => {
    movieApi.getTrending().then((res) => {
      setMovies(res.results);
    });
  }, [movies]);

  return (
    <Flex vertical align="center" >
      {movies?.map((movie) => (
          <MovieCard movie={movie}/>
      ))}
    </Flex>
  );
}

export default Trending;