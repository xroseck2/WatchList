import { Router } from 'express';
import { playlistsController } from './controller';

/**
 * @swagger
 * tags:
 *   name: Playlists
 *   description: The playlists managing API
 * components:
 *   schemas:
 *     Playlist:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the playlist
 *         name:
 *           type: string
 *           description: The name of the playlist
 *         moviesIds:
 *           type: array
 *           items:
 *             type: string
 *             description: The IDs of movies in the playlist
 *         club:
 *           $ref: '#/components/schemas/Club'
 *           description: The club that the playlist is in
 *         owner:
 *           $ref: '#/components/schemas/User'
 *           description: The owner of the playlist
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The date the playlist was added
 *     CreatePlaylistRequest:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         name:
 *           type: string
 *           minLength: 1
 *           maxLength: 255
 *           description: The name of the playlist
 *         movieIds:
 *           type: array
 *           items:
 *             type: string
 *             description: The IDs of movies in the playlist
 *     UpdatePlaylistRequest:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           minLength: 1
 *           maxLength: 255
 *           description: The name of the playlist
 *     UpdatePlaylistMoviesRequest:
 *       type: object
 *       required:
 *         - movieIds
 *       properties:
 *         movieIds:
 *           type: array
 *           items:
 *             type: string
 *             description: The IDs of movies in the playlist
 */
export const playlistsRouter = Router();

/**
 * @swagger
 * /api/v1/playlists:
 *   get:
 *     summary: Return page with playlists
 *     tags: [Playlists]
 *     parameters:
 *       - in: query
 *         name: size
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The number of playlists to return
 *           required: false
 *       - in: query
 *         name: page
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The page number to return
 *           required: false
 *     responses:
 *       200:
 *         description: A list of playlists.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Playlist'
 *       500:
 *         description: Some server error
 */
playlistsRouter.get('/', playlistsController.getPlaylists);

/**
 * @swagger
 * /api/v1/playlists/search:
 *   get:
 *     summary: Search for a playlist by ID, name, userId or clubId
 *     tags: [Playlists]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the playlist to search
 *         required: false
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *           description: The name of the playlist to search
 *         required: false
 *       - in: query
 *         name: userId
 *         schema:
 *           type: string
 *           description: The userId associated with the playlist to search
 *         required: false
 *       - in: query
 *         name: clubId
 *         schema:
 *           type: string
 *           description: The clubId associated with the playlist to search
 *         required: false
 *     responses:
 *       200:
 *         description: The playlist was successfully found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Playlist'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID, name, userId or clubId did not match any existing playlist
 *       500:
 *         description: Some server error
 */
playlistsRouter.get('/search', playlistsController.getPlaylist);

/**
 * @swagger
 * /api/v1/playlists:
 *   post:
 *     summary: Create a new playlist
 *     tags: [Playlists]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreatePlaylistRequest'
 *     responses:
 *       201:
 *         description: The playlist was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Playlist'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       500:
 *         description: Some server error
 */
playlistsRouter.post('/', playlistsController.createPlaylist);

/**
 * @swagger
 * /api/v1/playlists/{id}:
 *   delete:
 *     summary: Delete a playlist
 *     tags: [Playlists]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the playlist to delete
 *           required: true
 *     responses:
 *       200:
 *         description: The playlist was successfully deleted
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Playlist'
 *       404:
 *         description: Bad request, the provided ID did not match any existing playlist
 *       500:
 *         description: Some server error
 */
playlistsRouter.delete('/:id', playlistsController.deletePlaylist);

/**
 * @swagger
 * /api/v1/playlists/{id}:
 *   put:
 *     summary: Update a playlist
 *     tags: [Playlists]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the playlist to update
 *           required: true
 *     requestBody:
 *       required: true
 *       content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UpdatePlaylistRequest'
 *     responses:
 *       200:
 *         description: The playlist was successfully updated
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Playlist'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing playlist
 *       500:
 *         description: Some server error
 */
playlistsRouter.put('/:id', playlistsController.updatePlaylist);

/**
 * @swagger
 * /api/v1/playlists/{id}/add_movies:
 *   put:
 *     summary: Add movies to a playlist
 *     tags: [Playlists]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the playlist to update
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdatePlaylistMoviesRequest'
 *     responses:
 *       200:
 *         description: The movies were successfully added to the playlist
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Playlist'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing playlist
 *       500:
 *         description: Some server error
 */
playlistsRouter.put('/:id/add_movies', playlistsController.addMovies);

/**
 * @swagger
 * /api/v1/playlists/{id}/remove_movies:
 *   put:
 *     summary: Remove movies from a playlist
 *     tags: [Playlists]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the playlist to update
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdatePlaylistMoviesRequest'
 *     responses:
 *       200:
 *         description: The movies were successfully removed from the playlist
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Playlist'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing playlist
 *       500:
 *         description: Some server error
 */
playlistsRouter.put('/:id/remove_movies', playlistsController.removeMovies);
