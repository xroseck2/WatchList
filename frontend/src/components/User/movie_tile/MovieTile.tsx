import {FC, useContext, useEffect, useState} from "react";
import {cn} from "../../../utils.ts";
import {Button, Flex, Modal, Typography} from "antd";
const {Text} = Typography;
import "./movie_tile.css"
import ModalMovieRow from "../../ModalMovieRow/ModalMovieRow.tsx";
import {UserContext, UserContextType} from "../../../contexts/UserContext.tsx";
import playlistApi from "../../../api/playlistApi.ts";
import userApi from "../../../api/userApi.ts";
import {Playlist} from "../../../models/playlist.ts";

type MovieProps = {
    movie: {
        id: string,
        title: string,
        descriptions: string,
        year: number,
        rating: number,
    }
    className?: string;
}

const MovieTile: FC<MovieProps> = ({className, movie}) => {
    const {user} = useContext<UserContextType>(UserContext);
    const [isOpen, setIsOpen] = useState(false);
    const [playlists, setPlaylists] = useState<Playlist[] | null>(null);
    console.log(user);
    const getContent = () => {
        if (!playlists) {
            return <Text>No playlists</Text>;
        }
        return playlists
            .filter((p) => !p.moviesIds.includes(movie.id))
            .map((v) =>
            <ModalMovieRow key={v.id} movieId={null} title={v.name} year={v.moviesIds.length}
                           setMovie={() => {}}
                           closeDialog={() => {
                   playlistApi.updateAddMovies(v.id, {movieIds: [movie.id]});
                   setIsOpen(false);
                   Modal.success({
                       content: 'Movie successfully added to playlist: ' + v.name,
                   });
            }}/>
        );
    }

    const showSelector = () => {
        setIsOpen(true);
    }
    useEffect(() => {
        userApi.getSingleById(user.id).then((user) => {
            setPlaylists(user.playlists);
        })
    }, [user.id]);
    return (
        <Flex vertical className={cn(className, "class-name box bg_black")}>
            <Flex justify="space-between">
                <Flex wrap>
                    <Text strong className="movie__heading">
                        {movie.title}
                    </Text>
                    <Text className="movie__heading"> ({movie.year})</Text>
                </Flex>
                <Flex align="center">
                    <Text className="movie__rating">{movie.rating.toFixed(1)}/10</Text>
                </Flex>
            </Flex>
            {
                user != null &&
                <Flex justify="end">
                    <Button style={{width: "10rem"}} onClick={showSelector}>Add to playlist</Button>
                </Flex>
            }
            <p>
                {movie.descriptions}
            </p>
            <Modal title="Select movie" open={isOpen} onCancel={() => setIsOpen(false)}
                   width="fit-content" footer={[]}
                   className="add__modal">{getContent()}</Modal>
        </Flex>
    )
}

export default MovieTile;