import {Button, Form, FormProps, Input, Modal} from "antd";
import {useNavigate} from "react-router-dom";
import "./register_page.css"
import userApi from "../../api/userApi.ts";
import {CreateUser} from "../../models/user.ts";

type FieldType = {
  username?: string;
  email?: string;
  password?: string;
  confPassword?: string;
};

export default function RegisterPage() {
  const navigate = useNavigate();
  const error = (message: string) => {
    Modal.error({
      title: 'Register failed',
      content: message,
    });
  };

  const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
    if (values.password == values.confPassword) {
      const req: CreateUser = {name: values.username!, email: values.email!, password: values.password!}
      userApi.createSingle(req)
        .then(function (res){
          if (res.email != undefined){
            console.log("Successfully logged in");
            navigate('/login');
            Modal.success({
              title: 'Register succeeded',
              content: "Account created",
            });
          } else {
            error(`User with email "${values.email}" or name "${values.username}" already exists`)
          }
        })
        .catch(function (err) {
          // Zod validation failed or duplicate email
          error(err.response.data.message);
          console.log(err);
        })
    } else {
      error("Passwords do not match");
      console.log("Passwords not the same")
    }
  };

  const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <Form
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18}}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        className="register__box"
      >
        <Form.Item<FieldType>
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<FieldType>
          label="Email"
          name="email"
          rules={[{type:"email", required: true, message: 'Please input your email!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<FieldType>
          label="Password"
          name="password"
          rules={[{min: 10, required: true, message: 'Password is too short' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item<FieldType>
          label="Confirm password"
          name="confPassword"
          rules={[{min: 10, required: true, message: 'Password is too short' }]}
        >
          <Input.Password />
        </Form.Item>
        <Button onClick={() => navigate("/login")}
                type="text">Already have an account? Login in
        </Button>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}