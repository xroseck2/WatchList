import { Prisma } from '@prisma/client';
import { Result } from '@badrap/result';
import prisma from '../../client';
import { ConflictError, NotFoundError } from '../../utils';
import { ClubEditData, ClubPlaylistsData, ClubReadResult } from './types/data';

export const updateClub = async (data: ClubEditData): ClubReadResult => {
  const updateData: Prisma.UserUpdateInput = {
    ...(data.name && { name: data.name }),
    ...(data.ownerId && { owner: { connect: { id: data.ownerId } } }),
  };

  try {
    const result = await prisma.club.update({
      where: {
        id: data.id,
      },
      data: updateData,
    });
    if (!result) {
      return Result.err(new NotFoundError('No clubs found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const addPlaylistsToClub = async (data: ClubPlaylistsData): ClubReadResult => {
  try {
    const updatedClub = await prisma.club.update({
      where: {
        id: data.id,
      },
      data: {
        playlists: {
          connect: data.playlistIds.map((playlistId) => ({ id: playlistId })),
        },
      },
      include: {
        playlists: true,
      },
    });

    return Result.ok(updatedClub);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`club repository addPlaylistToClub error${error.message}`));
    }
    return Result.err(error as Error);
  }
};

export const deletePlaylistsFromClub = async (data: ClubPlaylistsData): ClubReadResult => {
  try {
    const updatedClub = await prisma.club.update({
      where: { id: data.id },
      data: {
        playlists: {
          disconnect: data.playlistIds.map((playlistId) => ({ id: playlistId })),
        },
      },
      include: {
        playlists: true,
      },
    });

    return Result.ok(updatedClub);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`club repository deletePlaylistFromClub error${error.message}`));
    }
    return Result.err(error as Error);
  }
};
