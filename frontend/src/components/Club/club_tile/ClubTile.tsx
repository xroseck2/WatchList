import './club_tile.css'
import {FC, useEffect, useState} from "react";
import {cn} from "../../../utils.ts";
import {Flex, Typography} from "antd";
import {Link} from "react-router-dom";
import {User} from "../../../models/user.ts";
import userApi from "../../../api/userApi.ts";

const {Text} = Typography;

type ClubProps = {
    club: {
        name: string,
        ownerId: string,
        createAt: string,
        id: string,
    }
    className?: string;
}

const ClubTile: FC<ClubProps> = ({className, club}) => {
    const [owner, setOwner] = useState<User | null>(null);
    useEffect(() => {
        userApi.getSingleById(club.ownerId).then((v) => setOwner(v));
    }, [club.ownerId]);

    return (
        <Link className="club-tile" to={"/club/" + club.id}>
            <Flex vertical className={cn(className, "class-name box bg_black")}>
                <Text strong style={{fontSize: "x-large"}}>{club.name}</Text>
                <Text strong>Owner: {owner ? owner.name : "Loading"}</Text>
                <Text strong>Created at: {club.createAt}</Text>
            </Flex>
        </Link>
    )
}

export default ClubTile;
