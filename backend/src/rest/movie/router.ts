import { Router } from 'express';
import { moviesController } from './controller';

/**
 * @swagger
 * tags:
 *   name: Movies
 *   description: The movies managing API
 * components:
 *   schemas:
 *     SimplifiedExternalMovie:
 *       type: object
 *       properties:
 *         backdrop_path:
 *           type: string
 *           description: The backdrop path of the movie image
 *         id:
 *           type: integer
 *           description: The ID of the movie
 *         original_title:
 *           type: string
 *           description: The original title of the movie
 *         overview:
 *           type: string
 *           description: The overview of the movie
 *         title:
 *           type: string
 *           description: The title of the movie
 *         original_language:
 *           type: string
 *           description: The original language of the movie
 *         release_date:
 *           type: string
 *           format: date
 *           description: The release date of the movie
 *     ComplexExternalMovie:
 *       type: object
 *       properties:
 *         backdrop_path:
 *           type: string
 *           description: The backdrop path of the movie image
 *         budget:
 *           type: integer
 *           description: The budget of the movie
 *         genres:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               id:
 *                 type: integer
 *                 description: The ID of the genre
 *               name:
 *                 type: string
 *                 description: The name of the genre
 *         id:
 *           type: integer
 *           description: The ID of the movie
 *         imdb_id:
 *           type: string
 *           description: The IMDB ID of the movie
 *         original_language:
 *           type: string
 *           description: The original language of the movie
 *         original_title:
 *           type: string
 *           description: The original title of the movie
 *         overview:
 *           type: string
 *           description: The overview of the movie
 *         poster_path:
 *           type: string
 *           description: The poster path of the movie
 *         release_date:
 *           type: string
 *           format: date
 *           description: The release date of the movie
 *         revenue:
 *           type: integer
 *           description: The revenue of the movie
 *         status:
 *           type: string
 *           description: The status of the movie
 *         tagline:
 *           type: string
 *           description: The tagline of the movie
 *         title:
 *           type: string
 *           description: The title of the movie
 */
export const moviesRouter = Router();

/**
 * @swagger
 * /api/v1/movies:
 *   get:
 *     summary: Returns movie details by ID from an EXTERNAL database
 *     tags: [Movies]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the movie
 *           required: true
 *     responses:
 *       200:
 *         description: The details of the movie that matches the ID.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ComplexExternalMovie'
 *       500:
 *         description: Some server error
 */
moviesRouter.get('/', moviesController.getMovieDetails);

/**
 * @swagger
 * /api/v1/movies/trending:
 *   get:
 *     summary: Returns trending movies of the day from an EXTERNAL database
 *     tags: [Movies]
 *     responses:
 *       200:
 *         description: A list of trending movies.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/SimplifiedExternalMovie'
 *       500:
 *         description: Some server error
 */
moviesRouter.get('/trending', moviesController.getTrendingMovies);

/**
 * @swagger
 * /api/v1/movies/search:
 *   get:
 *     summary: Search for movies by title in EXTERNAL database
 *     tags: [Movies]
 *     parameters:
 *       - in: query
 *         name: title
 *         schema:
 *           type: string
 *           minLength: 1
 *           description: The title of the movie to search for
 *           required: true
 *       - in: query
 *         name: page
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The page number to return
 *           required: true
 *     responses:
 *       200:
 *         description: A list of movies that match the search criteria.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/SimplifiedExternalMovie'
 *       500:
 *         description: Some server error
 */
moviesRouter.get('/search', moviesController.searchMovies);
