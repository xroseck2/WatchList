export type ReqPagination = {
  size: number;
  page: number;
};
