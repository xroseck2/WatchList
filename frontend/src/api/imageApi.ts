// This is not api, this is just a function to complete image path

const DEFAULT_PATH = "https://image.tmdb.org/t/p/original"
function getImagePath(pathEnd: string) {
  return DEFAULT_PATH + pathEnd;
}

const ImageApi = {
  getImagePath
}

export default ImageApi