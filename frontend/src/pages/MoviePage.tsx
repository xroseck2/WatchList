import MovieTile from "../components/User/movie_tile/MovieTile.tsx";
import {useParams} from "react-router-dom";
import {Typography} from "antd";
import ReviewTile from "../components/User/review_tile/ReviewTile.tsx";
import {useEffect, useMemo, useState} from "react";
import { MovieDetailed} from "../models/movie.ts";
import movieApi from "../api/movieApi.ts";
import {Review} from "../models/review.ts";
import reviewApi from "../api/reviewApi.ts";
const {Text} = Typography;

export default function MoviePage() {
    const { movieId } = useParams();
    const [movie, setMovie] = useState<MovieDetailed>();
    const [reviews, setReviews] = useState<Review[]>([]);

    const data = useMemo(() => {
        return movie ? (
          <>
              <MovieTile movie={{title: movie!.title, id: String(movie.id),
                  rating: reviews.reduce((acc, review) => acc + review.rating, 0) / reviews.length,
                  year: Number.parseInt(movie!.release_date.substring(0, 4)),
                  descriptions: movie!.overview}}/>

              <hr className="line"/>
              <Text className="user__review_heading">REVIEWS</Text>
              <hr className="line"/>
              {reviews.map((value) => (
                <ReviewTile key={value.id} review={value} userId={value.userId} />
              ))}
          </>
        ) : (
          <div>
              Loading...
          </div>
        )
      }, [movie, reviews]);

    useEffect(() => {
        movieApi.getSingleById(movieId!).then((res) => {
            setMovie(res);
        });
    }, [movieId]);

    useEffect(() => {
        reviewApi.getSingleByMovie(movieId!).then((res) => {
            setReviews(res);
        });
    }, [movieId]);

    return (
            data
    );
}