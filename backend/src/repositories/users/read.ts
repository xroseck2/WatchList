import { Result } from '@badrap/result';
import prisma from '../../client';
import { PAGE_SIZE, Pagination } from '../../types';
import {
  PasswordHashResult,
  UserIdentificationData,
  UserReadManyResult,
  UserReadResult,
} from './types/data';
import { NotFoundError } from '../../utils';

export const findUserById = async (id: string): UserReadResult => {
  try {
    const result = await prisma.user.findUnique({
      where: {
        id,
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
        ownedClubs: true,
        clubs: true,
        reviews: true,
        playlists: true,
        posts: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such user'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findUserByEmail = async (email: string): UserReadResult => {
  try {
    const result = await prisma.user.findUnique({
      where: {
        email,
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
        ownedClubs: true,
        clubs: true,
        reviews: true,
        playlists: true,
        posts: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such user'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const findUserByName = async (name: string): UserReadManyResult => {
  try {
    const result = await prisma.user.findMany({
      where: {
        name: {
          contains: name,
        },
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
        ownedClubs: true,
        clubs: true,
        reviews: true,
        playlists: true,
        posts: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No such user'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const getAllUsers = async (pagination?: Pagination): UserReadManyResult => {
  const size = pagination?.size ?? PAGE_SIZE;
  const page = pagination?.page ?? 1;
  try {
    const result = await prisma.user.findMany({
      skip: (page - 1) * size,
      take: size,
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No users found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const getPasswordHash = async (data: UserIdentificationData): PasswordHashResult => {
  try {
    let result;

    if (data.id) {
      result = await prisma.user.findUnique({
        where: { id: data.id },
        select: { password: true },
      });
    } else if (data.name) {
      result = await prisma.user.findUnique({
        where: { name: data.name },
        select: { password: true },
      });
    } else if (data.email) {
      result = await prisma.user.findUnique({
        where: { email: data.email },
        select: { password: true },
      });
    }

    if (!result) {
      return Result.err(new NotFoundError('No such user'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};
