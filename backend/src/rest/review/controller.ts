import { Request, Response } from 'express';
import { handleRepositoryErrors, parseRequest } from '../../utils';
import repository from '../../repositories/reviews/index';
import {
  queryPaginationRequestSchema,
  paramsIdRequestSchema,
} from '../generalValidationSchemas';
import {
  createReviewRequestSchema,
  updateReviewRequestSchema,
  searchReviewRequestSchema,
} from './validationSchemas';

const getReviews = async (req: Request, res: Response) => {
  const request = await parseRequest(queryPaginationRequestSchema, req, res);
  if (request === null) return;

  const result = await repository.getAllReviews(request.query);
  if (result.isErr) {
    handleRepositoryErrors(result.error, res);
    return;
  }

  res.send(result.value);
};

const createReview = async (req: Request, res: Response) => {
  const request = await parseRequest(createReviewRequestSchema, req, res);
  if (request === null) return;

  const createdReview = await repository.createReview(request.body);
  if (createdReview.isErr) {
    handleRepositoryErrors(createdReview.error, res);
    return;
  }

  res.status(201).send(createdReview.value);
};

const deleteReview = async (req: Request, res: Response) => {
  const request = await parseRequest(paramsIdRequestSchema, req, res);
  if (request === null) return;

  const deletedReview = await repository.deleteReview({ id: request.params.id });
  if (deletedReview.isErr) {
    handleRepositoryErrors(deletedReview.error, res);
    return;
  }

  res.send(deletedReview.value);
};

const updateReview = async (req: Request, res: Response) => {
  const request = await parseRequest(updateReviewRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedReview = await repository.updateReview(updateData);
  if (updatedReview.isErr) {
    handleRepositoryErrors(updatedReview.error, res);
    return;
  }

  res.send(updatedReview.value);
};

const getReview = async (req: Request, res: Response) => {
  const request = await parseRequest(searchReviewRequestSchema, req, res);
  if (request === null) return;

  const { id, userId, movieId } = request.query;

  let review;
  if (id !== undefined) {
    review = await repository.findReviewById(id);
  } else if (userId !== undefined) {
    review = await repository.findReviewByUser(userId);
  } else if (movieId !== undefined) {
    review = await repository.findReviewByMovie(movieId);
  }

  if (review?.isErr) {
    handleRepositoryErrors(review.error, res);
    return;
  }

  res.send(review?.value);
};

export const reviewsController = {
  getReviews,
  createReview,
  deleteReview,
  updateReview,
  getReview,
};
