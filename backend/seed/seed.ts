import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';
import generateData from './data';
import { SALT_ROUNDS, SEED_COUNT } from '../src/types';

const prisma = new PrismaClient();

const seed = async () => {
  console.log('Start seeding ...');

  const data = await generateData(SEED_COUNT);

  for (let i = 0; i < data.users.length; i += 1) {
    console.log('Fake credentials for login before hashing are:');
    console.log(data.users[i]!.email);
    console.log(data.users[i]!.password);
  }

  const hashedUsers = await Promise.all(data.users.map(async (user) => {
    const hashedPassword = await bcrypt.hash(user.password, SALT_ROUNDS);
    return { ...user, password: hashedPassword };
  }));

  const operations = [];

  operations.push(prisma.user.createMany({ data: hashedUsers }));
  operations.push(prisma.club.createMany({ data: data.clubs }));
  operations.push(prisma.review.createMany({ data: data.reviews }));
  operations.push(prisma.playlist.createMany({ data: data.playlists }));
  operations.push(prisma.post.createMany({ data: data.posts }));

  for (let i = 0; i < SEED_COUNT; i += 1) {
    operations.push(prisma.playlist.update({
      where: {
        id: data.playlists[i]!.id!,
      },
      data: {
        user: {
          connect: { id: data.users[i]!.id! },
        },
      },
    }));
  }

  await prisma.$transaction(operations);

  console.log('Seeding finished.');
};

seed()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
  });
