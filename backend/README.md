## MovieMates backend

To run this humble app, you first need to create a `.env` file for environment variables. You can use the provided `.env.example` as a template.

Next, start the database by running:

```
docker run --detach -p 5555:5432 --name movie-mates-database -e POSTGRES_PASSWORD=admin -e POSTGRES_USER=admin -e POSTGRES_DB=database postgres:latest
```

Alternatively, you can run the `docker-compose.yml` file:

```
docker compose up
```

Once the database is up and running, you need to seed it:

```
npx prisma migrate dev --name 'NAME_OF_MY_MIGRATION'

# Prisma studio - look into the running database
npx prisma studio
```

With the database ready, you can now run the app:

```
npm i

npm run start
```

The backend will be listening on the port specified in your `.env` file. <br />

Also, you can find the REST API documentation at **/api-docs**.
