import {BaseModelId, CreatedAtModel} from "./base.ts";
import {User} from "./user.ts";
import {Movie} from "./movie.ts";

type BaseReview = {
  rating: number;
  text: string;
}

export type Review = (BaseReview & (BaseModelId & CreatedAtModel)) & {
  movie: Movie;
  user: User;
};

export type CreateReview = BaseReview & {
  userId: string;
  movieId: string;
}

export type UpdateReview = BaseReview & BaseModelId
