import {BaseModelId, CreatedAtModel} from "./base.ts";
import {User} from "./user.ts";
import {Club} from "./club.ts";

export type Post = (BaseModelId & CreatedAtModel) & {
  author: User;
  message: string;
  club: Club;
}

export type CreatePost = {
  message: string;
  authorId: string;
  clubId: string;
}