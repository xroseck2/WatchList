import { Result } from '@badrap/result';
import { Prisma } from '@prisma/client';
import prisma from '../../client';
import { UserCreateData, UserReadResult } from './types/data';
import { ConflictError } from '../../utils';

const createUser = async (data: UserCreateData): UserReadResult => {
  try {
    const initialPlaylists: Prisma.PlaylistCreateWithoutUserInput[] = [
      {
        name: 'Favourites',
      },
      {
        name: 'Seen',
      },
      {
        name: 'To Watch',
      },
    ];
    const newUser = await prisma.user.create({
      data: {
        name: data.name,
        email: data.email,
        password: data.password,
        playlists: {
          create: initialPlaylists,
        },
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
      },
    });
    return Result.ok(newUser);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      if (error.code === 'P2002') {
        return Result.err(new ConflictError(`User with email: ${data.email} or name: ${data.name} already exists`));
      }
    }
    return Result.err(error as Error);
  }
};

export default createUser;
