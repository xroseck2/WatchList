import { z } from 'zod';

export const searchMoviesRequestSchema = z.object({
  query: z.object({
    title: z.string().min(1),
    page: z.coerce.number().positive(),
  }),
});

export const queryMovieIdRequestSchema = z.object({
  query: z.object({
    id: z.string(),
  }),
});
