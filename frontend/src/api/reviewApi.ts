import BaseApi from "./baseApi.ts";
import {ReqPagination} from "../models/request.ts";
import {CreateReview, Review, UpdateReview} from "../models/review.ts";

const REVIEW_PREFIX = "/reviews";

async function getSingleById(id: string) {
  return BaseApi.getSingleParams<Review>(`${REVIEW_PREFIX}/search`, {params: {id: id}});
}

async function getSingleByUser(userId: string) {
  return BaseApi.getAll<Review>(`${REVIEW_PREFIX}/search`, {params: {userId: userId}});
}

async function getSingleByMovie(movieId: string) {
  return BaseApi.getAll<Review>(`${REVIEW_PREFIX}/search`, {params: {movieId: movieId}});
}

async function getAllPaginated({ page }: ReqPagination) {
  return BaseApi.getAllPaginated<Review>(REVIEW_PREFIX, {
    params: { page },
  });
}

async function createSingle(payload: CreateReview) {
  return BaseApi.postSingle<Review>(REVIEW_PREFIX, payload);
}

async function updateSingle(id: string, payload: UpdateReview) {
  return BaseApi.putSingle<Review>(`${REVIEW_PREFIX}/${id}`, payload);
}

async function deleteSingle(id: string) {
  return BaseApi.deleteSingle<Review>(`${REVIEW_PREFIX}/${id}`);
}

const ReviewApi = {
  getSingleById,
  getSingleByUser,
  getSingleByMovie,
  getAllPaginated,
  createSingle,
  updateSingle,
  deleteSingle,
};

export default ReviewApi;
