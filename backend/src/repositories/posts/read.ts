import { Result } from '@badrap/result';
import prisma from '../../client';
import { PAGE_SIZE, Pagination } from '../../types';
import { NotFoundError } from '../../utils';
import { PostReadManyResult, PostReadResult } from './types/data';

const findPostById = async (id: string): PostReadResult => {
  try {
    const result = await prisma.post.findUnique({
      where: {
        id,
      },
      select: {
        id: true,
        message: true,
        authorId: true,
        clubId: true,
        createdAt: true,
        author: true,
        club: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError(`No post with id: ${id}`));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

const findPostByAuthorId = async (authorId: string, pagination?: Pagination): PostReadManyResult => {
  const size = pagination?.size ?? PAGE_SIZE;
  const page = pagination?.page ?? 1;
  try {
    const result = await prisma.post.findMany({
      where: {
        authorId,
      },
      skip: (page - 1) * size,
      take: size,
    });
    if (!result) {
      return Result.err(new NotFoundError(`User with id ${authorId} has no posts`));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

const findPostByClubId = async (clubId: string, pagination?: Pagination): PostReadManyResult => {
  const size = pagination?.size ?? PAGE_SIZE;
  const page = pagination?.page ?? 1;
  try {
    const result = await prisma.post.findMany({
      where: {
        clubId,
      },
      skip: (page - 1) * size,
      take: size,
    });
    if (!result) {
      return Result.err(new NotFoundError('No posts found in this club'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

const getAllPosts = async (pagination?: Pagination): PostReadManyResult => {
  const size = pagination?.size ?? PAGE_SIZE;
  const page = pagination?.page ?? 1;
  try {
    const result = await prisma.post.findMany({
      skip: (page - 1) * size,
      take: size,
    });
    if (!result) {
      return Result.err(new NotFoundError('No posts found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export {
  findPostById,
  findPostByAuthorId,
  findPostByClubId,
  getAllPosts,
};
