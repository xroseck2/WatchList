import {Button, Flex, Image, Typography} from "antd";
import {Link, useParams} from "react-router-dom";
import {useContext, useEffect, useState} from "react";
import clubApi from "../../api/clubApi.ts";
import {Club} from "../../models/club.ts";
import {UserContext, UserContextType} from "../../contexts/UserContext.tsx";
import {PlusOutlined, CloseCircleOutlined} from "@ant-design/icons";
import ClubTile from "../../components/Club/club_tile/ClubTile.tsx";
import {formatCreatedAt} from "../../utils.ts";
import "./club_page.css";
import reviewApi from "../../api/reviewApi.ts";
import {Review} from "../../models/review.ts";
import ReviewTile from "../../components/User/review_tile/ReviewTile.tsx";
import userApi from "../../api/userApi.ts";

const {Text} = Typography;
function ClubPage() {
    const {clubId} = useParams();
    const [refresh, setRefresh] = useState(false);
    const {user} = useContext<UserContextType>(UserContext);
    const [club, setClub] = useState<Club | null>(null);
    const [reviews, setReviews] = useState<Review[]>([]); // State to store reviews

    useEffect(() => {
        if (!clubId) {
            return;
        }
        clubApi.getSingleById(clubId).then(async (v) => {
            setClub(v);
            if (v.members) {
                const userIds = v.members.map(v => v.id);
                if (!userIds.includes(v.owner.id)) {
                    userIds.push(v.owner.id);
                }

                const fetchedReviews = await Promise.all(
                    userIds.map((memberId) => reviewApi.getSingleByUser(memberId))
                );
                setReviews(fetchedReviews.flat());
            }
        });
    }, [clubId, refresh]);
    console.log("ClubOwner " + club?.owner.id)
    console.log("User " + user?.id)
    console.log("rese" + club?.owner.id.match(String(user?.id)))

    return (
        <Flex vertical>
            <Flex align="center">
                {
                    club != null &&
                    <ClubTile club={{
                        name: club.name, ownerId: club.ownerId,
                        createAt: formatCreatedAt(club.createdAt), id: club.id
                    }}/>

                }
                {user != null &&
                club?.members.some((value) => value.id == user?.id) ?
                    <Button className="club_page--button" icon={<CloseCircleOutlined/>}
                            onClick={() => userApi.updateLeaveClub(user.id, {clubId: club?.id})
                                .then(() => setRefresh(!refresh))
                    }
                    /> :
                    <Button className="club_page--button" icon={<PlusOutlined/>}
                            onClick={() => {
                                userApi.updateJoinClub(user.id, {clubId: club?.id})
                                    .then(() => setRefresh(!refresh))
                            }}
                    />
                }
            </Flex>
            <Flex className="box" style={{flexWrap: "wrap"}}>
                <Text style={{paddingRight: "0.3rem"}}>Members:</Text>
                {club?.members.map((value, index, array) =>
                    <Link to={`/user/${value.id}`}>
                        <Text className="clickable">
                            {value.name}
                        </Text>
                        <Text style={{paddingRight: "0.3rem"}}>
                            {array.length != index + 1 ? "," : ""}
                        </Text>
                    </Link>
                )}
            </Flex>
            <hr className="line"/>
            <Text className="user__review_heading">REVIEWS</Text>
            <hr className="line"/>
            <Flex className="user__empty_reviews_container" vertical>
                {reviews.length == 0 && (
                    <Image
                        className="user__empty_reviews_image"
                        src="/big_text.png"
                        preview={false}
                    />
                )}
                {reviews.map((value, index) => {
                    return <ReviewTile key={index} review={value} userId={value.userId}/>;
                })}
            </Flex>
        </Flex>
    );
}

export default ClubPage;