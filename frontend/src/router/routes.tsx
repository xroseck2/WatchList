import { Navigate, type RouteObject } from "react-router-dom";
import HomePage from "../pages/HomePage/HomePage.tsx";
import BaseLayout from "../layouts/base_layout/BaseLayout.tsx";
import ErrorPage from "../pages/ErrorPage.tsx";
import MoviePage from "../pages/MoviePage.tsx";
import ClubsPage from "../pages/ClubsPage/ClubsPage.tsx";
import SearchPage from "../pages/SearchPage/SearchPage.tsx";
import AddReviewPage from "../pages/AddReviewPage/AddReviewPage.tsx";
import UserPage from "../pages/UserPage/UserPage.tsx";
import LoginPage from "../pages/LoginPage/LoginPage.tsx";
import RegisterPage from "../pages/RegisterPage/RegisterPage.tsx";
import ClubPage from "../pages/ClubPage/ClubPage.tsx";
import PlaylistPage from "../pages/PlaylistPage/PlaylistPage.tsx";


const mainLayoutRoutes: RouteObject[] = [
    {
        index: true,
        element: <Navigate to="./home" relative="path" />,
    },
    {
        path: 'home',
        Component: HomePage,
    },
    {
        path: 'clubs',
        Component: ClubsPage,
    },
    {
        path: 'club/:clubId',
        Component: ClubPage,
    },
    {
        path: 'playlist/:playlistId',
        Component: PlaylistPage,
    },
    {
        path: 'search',
        Component: SearchPage,
    },
    {
        path: 'add',
        Component: AddReviewPage,
    },
    {
        path: 'user/:userId',
        Component: UserPage,
    },
    {
        path: "movie/:movieId",
        Component: MoviePage,
    },
    {
        path: 'login',
        Component: LoginPage,
    },
    {
        path: 'register',
        Component: RegisterPage,
    }
]


const routes: RouteObject[] = [
    {
        path: '/',
        Component: BaseLayout,
        children: mainLayoutRoutes
    },
    {
        path: '*',
        Component: ErrorPage
    }
]

export default routes
