import {BaseModelId, CreatedAtModel} from "./base.ts";
import {User} from "./user.ts";
import {Playlist} from "./playlist.ts";
import {Post} from "./post.ts";

export type Club = (BaseModelId & CreatedAtModel) & {
  name: string;
  owner: User;
  members: User[];
  playlists: Playlist[];
  posts: Post[];
}

export type CreateClub = {
  name: string;
  ownerId: string;
}

export type ClubPlaylists = {
  playlistsIds: string[];
}