import { config } from 'dotenv';
import { env } from 'process';
import express from 'express';
import cors from 'cors';
import { clubsRouter } from './rest/club/router';
import { moviesRouter } from './rest/movie/router';
import { playlistsRouter } from './rest/playlist/router';
import { postsRouter } from './rest/post/router';
import { reviewsRouter } from './rest/review/router';
import { usersRouter } from './rest/user/router';

config();

const app = express();
const port = env.PORT ?? 3000;

const fs = require('fs');
const yaml = require('js-yaml');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const options = {
  definition: {
    openapi: '3.1.0',
    info: {
      title: 'MovieMates',
      version: '0.1.0',
      description: 'MovieMates - REST API',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'Support',
        url: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        email: 'support@moviemates.com',
      },
    },
    servers: [
      {
        url: `http://localhost:${port}`,
      },
    ],
  },
  apis: ['./**/router.ts'],
};

const specs = swaggerJsdoc(options);
fs.writeFileSync('./api-docs/api-docs.yaml', yaml.dump(specs));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/v1/clubs', clubsRouter);
app.use('/api/v1/movies', moviesRouter);
app.use('/api/v1/playlists', playlistsRouter);
app.use('/api/v1/posts', postsRouter);
app.use('/api/v1/reviews', reviewsRouter);
app.use('/api/v1/users', usersRouter);
app.use(
  '/api-docs',
  swaggerUi.serve,
  swaggerUi.setup(specs),
);

// Default routing to 404
app.use((_req, res) => {
  res.status(404).send('Not found');
});

if (env.NODE_ENV !== 'test') {
  app.listen(port, () => {
    console.log(
      `[${new Date().toISOString()}] MovieMates backend listening on port: ${port}`,
    );
  });
}
