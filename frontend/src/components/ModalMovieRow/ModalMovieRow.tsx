import {Button, Row, Typography} from "antd";
import { CheckOutlined } from '@ant-design/icons';
import "./modal_movie_row.css"

const Text = Typography;
interface ModalMovieRowProps {
    movieId: number | null;
    title: string;
    year: string | null;
    closeDialog: () => void;
    setMovie: (movie: {title: string, year: string, id: number}) => void;
}

function ModalMovieRow({title, year, movieId, setMovie, closeDialog}: ModalMovieRowProps) {
    const onClick = () => {
        if (movieId == null) {
            closeDialog();
            return;
        }
        setMovie({title: title, year: year, id:movieId});
        closeDialog();
        console.log(movieId);
    };
    return (
        <Row justify="space-between" align="middle" style={{paddingBottom: "0.7rem"}}>
            <Text className="modal_row__text">{title} ({year})</Text>
            <Button icon={<CheckOutlined/>} onClick={onClick}/>
        </Row>
    );
}

export default ModalMovieRow;