export type BaseModelId = {
  id: string;
};

export type CreatedAtModel = {
  createdAt: string;
};