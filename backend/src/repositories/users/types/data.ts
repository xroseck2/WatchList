import { DbResult } from '../../../types';

type User = {
  id: string;
  email: string;
  name: string;
  createdAt: Date;
};

type Hash = {
  password: string;
};

export type UserReadManyResult = DbResult<User[]>;
export type UserReadResult = DbResult<User>;
export type PasswordHashResult = DbResult<Hash>;

export type UserCreateData = {
  name: string,
  email: string,
  password: string
};

export type UserIdData = {
  id: string
};

export type UserEditData = UserIdData & {
  name?: string | undefined
  email?: string | undefined
  password?: string | undefined
};

export type UserPlaylistData = UserIdData & {
  playlistIds: string[]
};

export type UserClubData = UserIdData & {
  clubId: string
};

export type UserIdentificationData = {
  id?: string | undefined
  name?: string | undefined
  email?: string | undefined
};
