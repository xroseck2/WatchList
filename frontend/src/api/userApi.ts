import BaseApi from "./baseApi";
import { ReqPagination } from "../models/request";
import {CreateUser, User, UserClub, UserExtended, UserPlaylist} from "../models/user.ts";

const USER_PREFIX = "/users";

async function getSingleById(id: string) {
  return BaseApi.getSingleParams<UserExtended>(`${USER_PREFIX}/search`, {params: {id: id}});
}

async function getAllByName(name: string) {
  return BaseApi.getAll<UserExtended>(`${USER_PREFIX}/search`, {params: {name: name}});
}

async function getSingleByEmail(email: string) {
  return BaseApi.getSingleParams<UserExtended>(`${USER_PREFIX}/search`, {params: {email: email}});
}

async function getAllPaginated({ page }: ReqPagination) {
  return BaseApi.getAllPaginated<User>(USER_PREFIX, {
    params: { page },
  });
}

async function createSingle(payload: CreateUser) {
  return BaseApi.postSingle<User>(USER_PREFIX, payload);
}

async function updateSingle(id: string, payload: CreateUser) {
  return BaseApi.putSingle<User>(`${USER_PREFIX}/${id}`, payload);
}

async function updateAddPlaylist(id: string, payload: UserPlaylist) {
  return BaseApi.putSingle<UserExtended>(`${USER_PREFIX}/${id}/add_playlists`, payload);
}

async function updateRemovePlaylist(id: string, payload: UserPlaylist) {
  return BaseApi.putSingle<UserExtended>(`${USER_PREFIX}/${id}/remove_playlists`, payload);
}

async function updateJoinClub(id: string, payload: UserClub) {
  return BaseApi.putSingle<UserExtended>(`${USER_PREFIX}/${id}/join_club`, payload);
}

async function updateLeaveClub(id: string, payload: UserClub) {
  return BaseApi.putSingle<UserExtended>(`${USER_PREFIX}/${id}/leave_club`, payload);
}

async function deleteSingle(id: string) {
  return BaseApi.deleteSingle<User>(`${USER_PREFIX}/${id}`);
}

async function verifyUser(email: string, password: string) {
  return BaseApi.post(`${USER_PREFIX}/verify`, {
    email: email,
    password: password
  });
}

const UserApi = {
  getSingleById,
  getAllByName,
  getSingleByEmail,
  getAllPaginated,
  createSingle,
  updateSingle,
  updateAddPlaylist,
  updateRemovePlaylist,
  updateJoinClub,
  updateLeaveClub,
  deleteSingle,
  verifyUser,
};

export default UserApi;
