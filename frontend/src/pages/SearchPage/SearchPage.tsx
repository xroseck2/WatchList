import {useEffect, useState} from "react";
import Search from "antd/es/input/Search";
import {Col, Flex, Row, Typography} from "antd";
import "./search_page.css"
import SwitcherButton from "../../components/SwitcherButton/SwitcherButton.tsx";
import movieApi from "../../api/movieApi.ts";
import clubApi from "../../api/clubApi.ts";
import userApi from "../../api/userApi.ts";
import {Club} from "../../models/club.ts";
import {User} from "../../models/user.ts";
import {Movie} from "../../models/movie.ts";
import {Link} from "react-router-dom";

const {Text, Title} = Typography;
const MovieTile = (movie: Movie, primary: boolean) => {
    const name = primary ? "primary" : "secondary";
    return (
        <Link to={"/movie/" + movie.id}>
            <Row className={`tile--${name} box`} justify="space-between">
                <Text strong className={`tile__text--${name}`}>{movie.title}</Text>
                { movie.release_date && movie.release_date.length > 3 &&
                    <Text className={`tile__text--${name}`}>({movie.release_date.slice(0, 4)})</Text>
                }
            </Row>
        </Link>
    );
};

const UserTile = (user: User, primary: boolean) => {
    const name = primary ? "primary" : "secondary";
    return (
        <Link to={"/user/" + user.id}>
            <Row className={`tile--${name} box`} justify="space-between">
                <Text strong className={`tile__text--${name}`}>{user.name}</Text>
            </Row>
        </Link>
    );
};

const ClubTile = (club: Club, primary: boolean) => {
    const name = primary ? "primary" : "secondary";
    return (
        <Link to={"/club/" + club.id}>
            <Row className={`tile--${name} box`} justify="space-between">
                <Text strong className={`tile__text--${name}`}>{club.name}</Text>
            </Row>
        </Link>
    );
};

const NotFound = () => {
    return <Title> Not found </Title>;
}

export default function SearchPage() {
    const [search, setSearch] = useState(localStorage.getItem('search') ?? "");
    const [index, setIndex] = useState(0);

    const [movies, setMovies] = useState<Movie[]>([]);
    const [users, setUsers] = useState<User[]>([]);
    const [clubs, setClubs] = useState<Club[]>([]);
    const renderSwitch = () => {
        switch(index) {
            case 0:
                return movies.length > 0 ?
                    movies.map((value, index) => MovieTile(value, index % 2 == 1))
                    : NotFound();
            case 1:
                return users.length > 0 ?
                    users.map((value, index) => UserTile(value, index % 2 == 1))
                    : NotFound();
            case 2:
                return clubs.length > 0 ?
                    clubs.map((value, index) => ClubTile(value, index % 2 == 1))
                    : NotFound();
            default:
                return <h2>Error</h2>;
        }
    }

    useEffect(() => {
        localStorage.setItem('search', search);
        if (search.length > 0) {
            movieApi.getAllByName(search, 1).then(r => setMovies(r.results));
            userApi.getAllByName(search).then(r => setUsers(r));
            clubApi.getAllByName(search).then(r => setClubs(r));
        } else {
            clearSearch();
        }
    }, [search])

    const clearSearch = () => {
        localStorage.setItem('search', "");
        movieApi.getTrending().then(r => setMovies(r.results));
        userApi.getAllPaginated({page: 1, size: 15}).then(r => setUsers(r));
        clubApi.getAllPaginated({page: 1, size: 15}).then(r => setClubs(r));
    }
    if (search == "x") {
        clearSearch();
    }
    return (
        <Col>
            <Flex justify="center" className="top_padding">
                <Search className="search_page" allowClear
                    placeholder="Search recipient" size="large"
                    defaultValue={search}
                    onSearch={(value: string, _event, options)=> {
                    if (options?.source == "clear") {
                        clearSearch();
                        return;
                    }
                    setSearch(value);
                    }}
                />
            </Flex>

            <Row justify="space-around" className="top_padding">
                <SwitcherButton title={"Movies"} selected={index==0} setIndex={() => setIndex(0)}/>
                <SwitcherButton title={"Users"} selected={index==1} setIndex={() => setIndex(1)}/>
                <SwitcherButton title={"Clubs"} selected={index==2} setIndex={() => setIndex(2)}/>
            </Row>
            {
            renderSwitch()
        }
        </Col>
    );
}