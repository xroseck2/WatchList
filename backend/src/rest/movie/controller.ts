import { env } from 'process';
import { Request, Response } from 'express';
import { handleRepositoryErrors, parseRequest } from '../../utils';
import {
  searchMoviesRequestSchema,
  queryMovieIdRequestSchema,
} from './validationSchemas';

const axios = require('axios');

const getTrendingMovies = async (_req: Request, res: Response) => {
  try {
    const result = await axios.get('https://api.themoviedb.org/3/trending/movie/day', {
      headers: {
        Authorization: `Bearer ${env.MOVIE_DATABASE_TOKEN}`,
        accept: 'application/json',
      },
      params: {
        language: 'en-US',
      },
    });

    res.send(result.data);
  } catch (error) {
    handleRepositoryErrors(new Error(), res);
  }
};

const searchMovies = async (req: Request, res: Response) => {
  const request = await parseRequest(searchMoviesRequestSchema, req, res);
  if (request === null) return;

  try {
    const result = await axios.get('https://api.themoviedb.org/3/search/movie', {
      headers: {
        Authorization: `Bearer ${env.MOVIE_DATABASE_TOKEN}`,
        accept: 'application/json',
      },
      params: {
        query: request.query.title,
        include_adult: false,
        language: 'en-US',
        page: request.query.page,
      },
    });

    res.send(result.data);
  } catch (error) {
    handleRepositoryErrors(new Error(), res);
  }
};

const getMovieDetails = async (req: Request, res: Response) => {
  const request = await parseRequest(queryMovieIdRequestSchema, req, res);
  if (request === null) return;

  try {
    const result = await axios.get(`https://api.themoviedb.org/3/movie/${request.query.id}`, {
      headers: {
        Authorization: `Bearer ${env.MOVIE_DATABASE_TOKEN}`,
        accept: 'application/json',
      },
      params: {
        language: 'en-US',
      },
    });

    res.send(result.data);
  } catch (error) {
    handleRepositoryErrors(new Error(), res);
  }
};

export const moviesController = {
  getTrendingMovies,
  searchMovies,
  getMovieDetails,
};
