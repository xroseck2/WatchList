import { Review } from '@prisma/client';
import { DbResult } from '../../../types';

export type ReviewReadManyResult = DbResult<Review[]>;
export type ReviewReadResult = DbResult<Review>;

export type ReviewCreateData = {
  text: string,
  rating: number,
  userId: string,
  movieId: string
};

export type ReviewIdData = {
  id: string
};

export type ReviewEditData = ReviewIdData & {
  text?: string | undefined
  rating?: number | undefined
};
