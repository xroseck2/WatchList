import { Prisma } from '@prisma/client';
import { Result } from '@badrap/result';
import prisma from '../../client';
import { ConflictError, NotFoundError } from '../../utils';
import { PlaylistEditData, PlaylistMoviesData, PlaylistReadResult } from './types/data';

export const updatePlaylist = async (data: PlaylistEditData): PlaylistReadResult => {
  const updateData: Prisma.UserUpdateInput = {
    ...(data.name && { name: data.name }),
  };

  try {
    const result = await prisma.playlist.update({
      where: {
        id: data.id,
      },
      data: updateData,
    });
    if (!result) {
      return Result.err(new NotFoundError(`No playlist with id ${data.id} found`));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const addMoviesToPlaylist = async (data: PlaylistMoviesData): PlaylistReadResult => {
  try {
    const existingPlaylist = await prisma.playlist.findUnique({
      where: {
        id: data.id,
      },
    });

    if (!existingPlaylist) {
      return Result.err(new NotFoundError('No such playlist'));
    }

    const uniqueMovieIds = new Set([...existingPlaylist.moviesIds, ...data.movieIds]);

    const updatedPlaylist = await prisma.playlist.update({
      where: {
        id: data.id,
      },
      data: {
        moviesIds: Array.from(uniqueMovieIds),
      },
    });

    return Result.ok(updatedPlaylist);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`playlist repository addMovieToPlaylist error${error.message}`));
    }
    return Result.err(error as Error);
  }
};

export const deleteMoviesFromPlaylist = async (data: PlaylistMoviesData): PlaylistReadResult => {
  try {
    const existingPlaylist = await prisma.playlist.findUnique({
      where: {
        id: data.id,
      },
    });

    if (!existingPlaylist) {
      return Result.err(new NotFoundError('No such playlist'));
    }

    const remainingMovieIds = existingPlaylist.moviesIds.filter((movieId) => !data.movieIds.includes(movieId));

    const updatedPlaylist = await prisma.playlist.update({
      where: {
        id: data.id,
      },
      data: {
        moviesIds: remainingMovieIds,
      },
    });

    return Result.ok(updatedPlaylist);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`playlist repository deleteMovieFromPlaylist error${error.message}`));
    }
    return Result.err(error as Error);
  }
};
