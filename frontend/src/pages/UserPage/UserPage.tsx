import {Button, Flex, Image, Input, Modal, Typography} from "antd";
import {Link, useParams} from "react-router-dom";
import "./user_page.css";
import UserHeading from "../../components/User/user_heading/UserHeading.tsx";
import ReviewTile from "../../components/User/review_tile/ReviewTile.tsx";
import {useContext, useEffect, useState} from "react";
import userApi from "../../api/userApi.ts";
import {UserExtended} from "../../models/user.ts";
import {PlusOutlined} from "@ant-design/icons";
import playlistApi from "../../api/playlistApi.ts";
import {UserContext, UserContextType} from "../../contexts/UserContext.tsx";

const {Text} = Typography;

function UserPage() {
    const {userId} = useParams();
    const [pageUser, setPageUser] = useState<UserExtended | null>(null);
    const {user} = useContext<UserContextType>(UserContext);

    const [openModal, setOpenModal] = useState(false);
    const [playlistName, setPlaylistName] = useState("");
    const [refresh, setRefresh] = useState(false);
    const handleOk = async () => {
        setOpenModal(false);
        await playlistApi.createSingle({name: playlistName, movieIds: []})
            .then((newP) =>
                userApi.updateAddPlaylist(user?.id, {playlistIds: [newP.id]})
                    .then(() => setRefresh(!refresh))
            )
        setRefresh(!refresh);
        setPlaylistName("");
    };

    const handleCancel = () => {
        setOpenModal(false);
        setPlaylistName("");

    };

    useEffect(() => {
        userApi.getSingleById(userId ?? "error").then(value => {
            console.log(value);
            setPageUser(value);
        });
    }, [userId])
    if (pageUser == null) {
        return <Text>Not found</Text>
    }
    const createPlaylist = () => {
        setOpenModal(true);
    }

    return (
        <div className="bg_black">
            <Modal title="Insert name of new playlist" open={openModal} onOk={handleOk} onCancel={handleCancel}>
                <Input defaultValue={playlistName} onChange={(e) => setPlaylistName(e.target.value)}/>
            </Modal>
            <UserHeading username={pageUser.name} createdDate={pageUser.createdAt} id={pageUser.id}/>
            <Flex className="box" vertical>
                {
                    pageUser?.ownedClubs.length != 0 &&
                    <Flex wrap>
                        <Text className="user__club_text" strong>Owned clubs: </Text>
                        {pageUser?.ownedClubs.map((value, index, array) =>
                            <Link to={"/club/" + value.id}>
                                <Text className="user__club_text clickable">
                                    {value.name}{array.length != index + 1 ? "," : ""}
                                </Text>
                            </Link>
                        )}
                    </Flex>
                }
                {
                    pageUser?.clubs.length != 0 &&
                    <Flex wrap>
                        <Text className="user__club_text" strong>Joined clubs: </Text>
                        {pageUser?.clubs.map((value, index, array) =>
                            <Link to={"/club/" + value.id}>
                                <Text className="user__club_text clickable">
                                    {value.name}{array.length != index + 1 ? "," : ""}
                                </Text>
                            </Link>
                        )}
                    </Flex>
                }
                {
                    pageUser?.playlists.length != 0 &&
                    <Flex wrap>
                        <Text className="user__club_text" strong>
                            Playlists:
                        </Text>
                        {pageUser?.playlists.map((value, index, array) =>
                            <Link to={{pathname: "/playlist/" + value.id,}}>
                                <Text className="user__club_text clickable">
                                    {value.name} ({value.moviesIds.length}){array.length != index + 1 ? "," : ""}
                                </Text>
                            </Link>
                        )}
                        <Button icon={<PlusOutlined/>} onClick={createPlaylist} />
                    </Flex>
                }

            </Flex>
            <hr className="line" />
            <Text className="user__review_heading">REVIEWS</Text>
            <hr className="line" />
            <Flex className="user__empty_reviews_container" vertical>
                {pageUser.reviews.length == 0 && (
                    <Image
                        className="user__empty_reviews_image"
                        src="/big_text.png"
                        preview={false}
                    />
                )}
                {pageUser.reviews.slice().reverse().map((value, index) => {
                    return <ReviewTile key={pageUser.id + index} review={value} userId={pageUser.id}/>;
                })}
            </Flex>
        </div>
    );
}

export default UserPage;