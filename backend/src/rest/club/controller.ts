import { Request, Response } from 'express';
import { handleRepositoryErrors, parseRequest } from '../../utils';
import repository from '../../repositories/clubs/index';
import {
  queryPaginationRequestSchema,
  paramsIdRequestSchema,
} from '../generalValidationSchemas';
import {
  createClubRequestSchema,
  updateClubRequestSchema,
  updateClubPlaylistsRequestSchema,
  searchClubRequestSchema,
} from './validationSchemas';

const getClubs = async (req: Request, res: Response) => {
  const request = await parseRequest(queryPaginationRequestSchema, req, res);
  if (request === null) return;

  const result = await repository.getAllClubs(request.query);
  if (result.isErr) {
    handleRepositoryErrors(result.error, res);
    return;
  }

  res.send(result.value);
};

const createClub = async (req: Request, res: Response) => {
  const request = await parseRequest(createClubRequestSchema, req, res);
  if (request === null) return;

  const createdClub = await repository.createClub(request.body);
  if (createdClub.isErr) {
    handleRepositoryErrors(createdClub.error, res);
    return;
  }

  res.status(201).send(createdClub.value);
};

const deleteClub = async (req: Request, res: Response) => {
  const request = await parseRequest(paramsIdRequestSchema, req, res);
  if (request === null) return;

  const deletedClub = await repository.deleteClub({ id: request.params.id });
  if (deletedClub.isErr) {
    handleRepositoryErrors(deletedClub.error, res);
    return;
  }

  res.send(deletedClub.value);
};

const updateClub = async (req: Request, res: Response) => {
  const request = await parseRequest(updateClubRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedClub = await repository.updateClub(updateData);
  if (updatedClub.isErr) {
    handleRepositoryErrors(updatedClub.error, res);
    return;
  }

  res.send(updatedClub.value);
};

const getClub = async (req: Request, res: Response) => {
  const request = await parseRequest(searchClubRequestSchema, req, res);
  if (request === null) return;

  const { id, name } = request.query;

  let club;
  if (id !== undefined) {
    club = await repository.findClubById(id);
  } else if (name !== undefined) {
    club = await repository.findClubByName(name);
  }

  if (club?.isErr) {
    handleRepositoryErrors(club.error, res);
    return;
  }

  res.send(club?.value);
};

const addPlaylists = async (req: Request, res: Response) => {
  const request = await parseRequest(updateClubPlaylistsRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedClub = await repository.addPlaylistsToClub(updateData);
  if (updatedClub.isErr) {
    handleRepositoryErrors(updatedClub.error, res);
    return;
  }

  res.send(updatedClub.value);
};

const removePlaylists = async (req: Request, res: Response) => {
  const request = await parseRequest(updateClubPlaylistsRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedClub = await repository.deletePlaylistsFromClub(updateData);
  if (updatedClub.isErr) {
    handleRepositoryErrors(updatedClub.error, res);
    return;
  }

  res.send(updatedClub.value);
};

export const clubsController = {
  getClubs,
  createClub,
  deleteClub,
  updateClub,
  getClub,
  addPlaylists,
  removePlaylists,
};
