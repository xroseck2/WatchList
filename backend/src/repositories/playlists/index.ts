import { addMoviesToPlaylist, deleteMoviesFromPlaylist, updatePlaylist } from './edit';
import { deletePlaylist } from './delete';
import { createPlaylist, createPlaylistWithMovies } from './create';
import {
  findPlaylistByClub, findPlaylistById, findPlaylistByName, findPlaylistByUser, getAllPlaylists,
} from './read';

const playlistRepository = {
  createPlaylist,
  createPlaylistWithMovies,
  deletePlaylist,
  updatePlaylist,
  findPlaylistById,
  findPlaylistByName,
  findPlaylistByUser,
  findPlaylistByClub,
  getAllPlaylists,
  addMoviesToPlaylist,
  deleteMoviesFromPlaylist,
};

export default playlistRepository;
