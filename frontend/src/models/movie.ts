
export type MovieDetailed = {
  adult: boolean;
  genres: Genre[];
  id: number;
  imdb_id: string;
  origin_country: string[];
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  revenue: number;
  runtime: number;
  status: string;
  tagline: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export type AllMovies = {
  page: number;
  results: MovieDetailed[];
  total_pages: number;
  total_results: number;

}

export type Movie = {
  id: string;
  title: string;
  overview: string;
  release_date: string
}
type Genre = {
  id: number;
  name: string;
}
