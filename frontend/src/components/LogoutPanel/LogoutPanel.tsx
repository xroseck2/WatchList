import {useContext} from "react";
import {UserContext} from "../../contexts/UserContext.tsx";
import "./logout_panel.css";
import {Button} from "antd";

const LogoutPanel: React.FC = () => {
    const { user, setUser } = useContext(UserContext);

    const logout = () => {
        setUser(null);
    };

    return (
        <div className="logout__container">
            <h2>Current user:</h2>
            <h1>{user?.name} ({user?.email})</h1>
            <Button onClick={logout}>Logout</Button>
        </div>
    );
}

export default LogoutPanel;
