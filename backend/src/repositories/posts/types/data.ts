import { Post } from '@prisma/client';
import { DbResult } from '../../../types';

export type PostReadManyResult = DbResult<Post[]>;
export type PostReadResult = DbResult<Post>;

export type PostCreateData = {
  message: string
  authorId: string
  clubId: string
};
