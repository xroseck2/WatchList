import { Router } from 'express';
import { usersController } from './controller';

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: The users managing API
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the user
 *         email:
 *           type: string
 *           description: The email of the user
 *         name:
 *           type: string
 *           description: The name of the user
 *         ownedClubs:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Club'
 *           description: The clubs owned by the user
 *         clubs:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Club'
 *           description: The clubs the user is a member of
 *         reviews:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Review'
 *           description: The reviews written by the user
 *         playlists:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Playlist'
 *           description: The playlists created by the user
 *         posts:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Post'
 *           description: The posts written by the user
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The date the user was added
 *     CreateUserRequest:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         name:
 *           type: string
 *           minLength: 1
 *           maxLength: 255
 *           description: The name of the user
 *         email:
 *           type: string
 *           format: email
 *           description: The email of the user
 *         password:
 *           type: string
 *           minLength: 10
 *           maxLength: 255
 *           description: The password of the user
 *     UpdateUserRequest:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           minLength: 1
 *           maxLength: 255
 *           description: The name of the user
 *         email:
 *           type: string
 *           format: email
 *           description: The email of the user
 *         password:
 *           type: string
 *           minLength: 10
 *           maxLength: 255
 *           description: The password of the user
 *     updateUserPlaylistsRequest:
 *       type: object
 *       required:
 *         - playlistIds
 *       properties:
 *         playlistIds:
 *           type: array
 *           items:
 *             type: string
 *             description: The ID of the playlist
 *     updateUserClubsRequest:
 *       type: object
 *       required:
 *         - clubId
 *       properties:
 *         clubId:
 *           type: string
 *           description: The ID of the playlist
 *     verifyUserRequest:
 *       type: object
 *       required:
 *         - password
 *       properties:
 *         id:
 *           type: string
 *           description: The unique identifier of the user
 *         name:
 *           type: string
 *           description: The name of the user
 *           minLength: 1
 *           maxLength: 255
 *         email:
 *           type: string
 *           format: email
 *           description: The email of the user
 *         password:
 *           type: string
 *           description: The password of the user
 *           minLength: 10
 *           maxLength: 255
 */
export const usersRouter = Router();

/**
 * @swagger
 * /api/v1/users:
 *   get:
 *     summary: Return page with users
 *     tags: [Users]
 *     parameters:
 *       - in: query
 *         name: size
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The number of users to return
 *           required: false
 *       - in: query
 *         name: page
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The page number to return
 *           required: false
 *     responses:
 *       200:
 *         description: A list of users.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       500:
 *         description: Some server error
 */
usersRouter.get('/', usersController.getUsers);

/**
 * @swagger
 * /api/v1/users/search:
 *   get:
 *     summary: Search for a user by ID, name, or email
 *     tags: [Users]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the user to search
 *         required: false
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *           description: The name of the user to search
 *         required: false
 *       - in: query
 *         name: email
 *         schema:
 *           type: string
 *           description: The email of the user to search
 *         required: false
 *     responses:
 *       200:
 *         description: The user was successfully found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID, name, or email did not match any existing user
 *       500:
 *         description: Some server error
 */
usersRouter.get('/search', usersController.getUser);

/**
 * @swagger
 * /api/v1/users:
 *   post:
 *     summary: Create a new user
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateUserRequest'
 *     responses:
 *       201:
 *         description: The user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       500:
 *         description: Some server error
 */
usersRouter.post('/', usersController.createUser);

/**
 * @swagger
 * /api/v1/users/verify:
 *   post:
 *     summary: Verify a user
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/verifyUserRequest'
 *     responses:
 *       200:
 *         description: The user was successfully verified
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       401:
 *         description: Invalid credentials
 *       500:
 *         description: Some server error
 */
usersRouter.post('/verify', usersController.verifyUser);

/**
 * @swagger
 * /api/v1/users/{id}:
 *   delete:
 *     summary: Delete a user
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the user to delete
 *           required: true
 *     responses:
 *       200:
 *         description: The user was successfully deleted
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       404:
 *         description: Bad request, the provided ID did not match any existing user
 *       500:
 *         description: Some server error
 */
usersRouter.delete('/:id', usersController.deleteUser);

/**
 * @swagger
 * /api/v1/users/{id}:
 *   put:
 *     summary: Update a user
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the user to update
 *           required: true
 *     requestBody:
 *       required: true
 *       content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UpdateUserRequest'
 *     responses:
 *       200:
 *         description: The user was successfully updated
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing user
 *       500:
 *         description: Some server error
 */
usersRouter.put('/:id', usersController.updateUser);

/**
 * @swagger
 * /api/v1/users/{id}/add_playlists:
 *   put:
 *     summary: Add playlists to a user
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the user to update
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/updateUserPlaylistsRequest'
 *     responses:
 *       200:
 *         description: The playlists were successfully added to the user
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing user
 *       500:
 *         description: Some server error
 */
usersRouter.put('/:id/add_playlists', usersController.addPlaylists);

/**
 * @swagger
 * /api/v1/users/{id}/remove_playlists:
 *   put:
 *     summary: Remove playlists from a user
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the user to update
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/updateUserPlaylistsRequest'
 *     responses:
 *       200:
 *         description: The playlists were successfully removed from the user
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing user
 *       500:
 *         description: Some server error
 */
usersRouter.put('/:id/remove_playlists', usersController.removePlaylists);

/**
 * @swagger
 * /api/v1/users/{id}/join_club:
 *   put:
 *     summary: User joins a club
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the user
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/updateUserClubsRequest'
 *     responses:
 *       200:
 *         description: The user successfully joined the club
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing user
 *       500:
 *         description: Some server error
 */
usersRouter.put('/:id/join_club', usersController.joinClub);

/**
 * @swagger
 * /api/v1/users/{id}/leave_club:
 *   put:
 *     summary: User leaves a club
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the user
 *         required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/updateUserClubsRequest'
 *     responses:
 *       200:
 *         description: The user successfully left the club
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing user
 *       500:
 *         description: Some server error
 */
usersRouter.put('/:id/leave_club', usersController.leaveClub);
