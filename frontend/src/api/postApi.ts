import BaseApi from "./baseApi.ts";
import {ReqPagination} from "../models/request.ts";
import {CreatePost, Post} from "../models/post.ts";

const POST_PREFIX = "/posts";

async function getSingleById(id: string) {
  return BaseApi.getSingleParams<Post>(`${POST_PREFIX}/search`, {params: {id: id}});
}

async function getAllByAuthor(authorId: string) {
  return BaseApi.getAll<Post>(`${POST_PREFIX}/search`, {params: {authorId: authorId}});
}

async function getAllByClub(clubId: string) {
  return BaseApi.getAll<Post>(`${POST_PREFIX}/search`, {params: {clubId: clubId}});
}

async function getAllPaginated({ page }: ReqPagination) {
  return BaseApi.getAllPaginated<Post>(POST_PREFIX, {
    params: { page },
  });
}

async function createSingle(payload: CreatePost) {
  return BaseApi.postSingle<Post>(POST_PREFIX, payload);
}

const PostApi = {
  getSingleById,
  getAllByAuthor,
  getAllByClub,
  getAllPaginated,
  createSingle,
};

export default PostApi;
