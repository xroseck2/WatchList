import { z } from 'zod';
import { paramsIdRequestSchema } from '../generalValidationSchemas';

export const createReviewRequestSchema = z.object({
  body: z.object({
    text: z.string().min(1),
    rating: z.number().nonnegative().max(10),
    userId: z.string().cuid(),
    movieId: z.string(),
  }),
});

export const updateReviewRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      text: z.string().min(1).optional(),
      rating: z.number().nonnegative().max(10).optional(),
    }),
  }),
);

export const searchReviewRequestSchema = z.object({
  query: z.object({
    id: z.string().cuid().optional(),
    userId: z.string().cuid().optional(),
    movieId: z.string().optional(),
  }),
}).refine((data) => {
  const definedProps = Object.values(data.query).filter((prop) => prop !== undefined);
  return definedProps.length >= 1;
}, {
  message: "Either 'id', 'userId' or 'movieId' must be provided",
});
