import { z } from 'zod';

export const createPostRequestSchema = z.object({
  body: z.object({
    message: z.string().min(1),
    authorId: z.string().cuid(),
    clubId: z.string().cuid(),
  }),
});

export const searchPostRequestSchema = z.object({
  query: z.object({
    id: z.string().cuid().optional(),
    authorId: z.string().cuid().optional(),
    clubId: z.string().cuid().optional(),
    size: z.coerce.number().positive().optional(),
    page: z.coerce.number().positive().optional(),
  }),
}).refine((data) => {
  const { id, authorId, clubId } = data.query;
  const definedProps = [id, authorId, clubId].filter((prop) => prop !== undefined);
  return definedProps.length >= 1;
}, {
  message: "Either 'id', 'authorId', 'clubId' must be provided",
});
