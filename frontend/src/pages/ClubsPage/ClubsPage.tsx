import {Button, Flex, Input, Modal, Typography} from "antd";
import './clubs_page.css'
import ClubTile from "../../components/Club/club_tile/ClubTile.tsx";
import UserApi from "../../api/userApi.ts";
import {useContext, useState, useEffect} from "react";
import {UserContext, UserContextType} from "../../contexts/UserContext.tsx";
import {Club} from "../../models/club.ts";
import {formatCreatedAt} from "../../utils.ts";
import {PlusOutlined} from "@ant-design/icons";
import clubApi from "../../api/clubApi.ts";
import useWindowDimension from "../../hooks/useWindowDimension.tsx";

const {Title} = Typography;
const MIN_WIDTH = 400;
export default function ClubsPage() {
    const {user} = useContext<UserContextType>(UserContext);
    const [ownedClubs, setOwnedClubs] = useState<Club[]>([]);
    const [clubs, setClubs] = useState<Club[]>([]);
    const [openModal, setOpenModal] = useState(false);
    const [clubName, setClubName] = useState("");
    const [refresh, setRefresh] = useState(false);
    const {width} = useWindowDimension();
    const handleOk = () => {
        setOpenModal(false);
        console.log(clubName)
        clubApi.createSingle({name: clubName, ownerId: user.id}).then(() => setRefresh(!refresh))
        setClubName("");
    };

    const handleCancel = () => {
        setOpenModal(false);
        setClubName("");

    };
    useEffect(() => {
        if (user) {
            UserApi.getSingleByEmail(user.email).then((res) => {
                setOwnedClubs(res.ownedClubs);
                setClubs(res.clubs)
            });
        }
    }, [user, refresh]);
    if (user == null) {
        return <Title>You have to be logged in</Title>
    }
    return (
        <Flex vertical={MIN_WIDTH > width}>
            <Flex vertical align="center" style={{width: MIN_WIDTH > width ? "100%" : "50%"}}>
                <Flex align="center" justify="center">
                    <Title>Owned clubs</Title>
                    <Button className="clubs__add_button" icon={<PlusOutlined/>} onClick={() => setOpenModal(true)}/>
                </Flex>
                {ownedClubs.map((club: Club) => {
                    return <ClubTile key={club.id} club={{id: club.id, name: club.name, ownerId: club.ownerId, createAt: formatCreatedAt(club.createdAt)}} />
                })}
            </Flex>
            <Flex vertical align="center" style={{width: MIN_WIDTH > width ? "100%" : "50%"}}>
                <Title>Joined clubs</Title>
                {clubs.map((club: Club) => (
                    <ClubTile key={club.id} club={{id: club.id, name: club.name, ownerId: club.ownerId, createAt: formatCreatedAt(club.createdAt)}} />
                ))}
            </Flex>

            <Modal title="Insert club name" open={openModal} onOk={handleOk} onCancel={handleCancel}>
                <Input defaultValue={clubName} onChange={(e) => setClubName(e.target.value)}/>
            </Modal>
        </Flex>

    );
}
