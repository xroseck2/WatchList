import { Router } from 'express';
import { postsController } from './controller';

/**
 * @swagger
 * tags:
 *   name: Posts
 *   description: The posts managing API
 * components:
 *   schemas:
 *     Post:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the post
 *         author:
 *           $ref: '#/components/schemas/User'
 *           description: The author of the post
 *         message:
 *           type: string
 *           description: The message of the post
 *         club:
 *           $ref: '#/components/schemas/Club'
 *           description: The club that the post is in
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The date the post was added
 *     CreatePostRequest:
 *        type: object
 *        required:
 *          - message
 *          - authorId
 *          - clubId
 *        properties:
 *          message:
 *            type: string
 *            minLength: 1
 *            description: The message of the post
 *          authorId:
 *            type: string
 *            description: The ID of the author
 *          clubId:
 *            type: string
 *            description: The ID of the club
 */
export const postsRouter = Router();

/**
 * @swagger
 * /api/v1/posts:
 *   get:
 *     summary: Return page with posts
 *     tags: [Posts]
 *     parameters:
 *       - in: query
 *         name: size
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The number of posts to return
 *           required: false
 *       - in: query
 *         name: page
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The page number to return
 *           required: false
 *     responses:
 *       200:
 *         description: A list of posts.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Post'
 *       500:
 *         description: Some server error
 */
postsRouter.get('/', postsController.getPosts);

/**
 * @swagger
 * /api/v1/posts/search:
 *   get:
 *     summary: Search for a post by ID, authorId, or clubId with optional pagination
 *     tags: [Posts]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the post to search
 *         required: false
 *       - in: query
 *         name: authorId
 *         schema:
 *           type: string
 *           description: The author ID of the post to search
 *         required: false
 *       - in: query
 *         name: clubId
 *         schema:
 *           type: string
 *           description: The club ID of the post to search
 *         required: false
 *       - in: query
 *         name: size
 *         schema:
 *           type: number
 *           description: The number of posts to return per page
 *         required: false
 *       - in: query
 *         name: page
 *         schema:
 *           type: number
 *           description: The page number to return
 *         required: false
 *     responses:
 *       200:
 *         description: The post was successfully found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Post'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID, authorId, or clubId did not match any existing post
 *       500:
 *         description: Some server error
 */
postsRouter.get('/search', postsController.getPost);

/**
 * @swagger
 * /api/v1/posts:
 *   post:
 *     summary: Create a new post
 *     tags: [Posts]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreatePostRequest'
 *     responses:
 *       201:
 *         description: The post was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Post'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       500:
 *         description: Some server error
 */
postsRouter.post('/', postsController.createPost);
