import {Button, Flex, Typography} from "antd";
import { MovieDetailed} from "../../models/movie.ts";
import imageApi from "../../api/imageApi.ts";
import "../../index.css";
import "./movie_card.css";
import {Link} from "react-router-dom";
import React from "react";
import useWindowDimension from "../../hooks/useWindowDimension.tsx";
import {CloseOutlined} from "@ant-design/icons";
const {Text, Paragraph} = Typography;

type MovieCardProps = {
  movie: MovieDetailed;
  removeFromPlaylist?: () => void;
}

const MIN_WIDTH = 685;
const MovieCard: React.FC<MovieCardProps> = ({movie, removeFromPlaylist}) => {
  const {width} = useWindowDimension()
  return (
        <Flex className="box" align="start" vertical={width < MIN_WIDTH}
              style={{width: width < MIN_WIDTH ? "21.3rem" : "", maxWidth: "750px"}}>
            <Link to={`/movie/${movie.id}`}>
                <img className="movie_card__image" src={imageApi.getImagePath(movie.poster_path)} alt={movie.title}/>
            </Link>
            <Flex vertical style={{padding: "0.5rem"}}>
                <Flex align="center" justify="space-between">
                    <Link to={`/movie/${movie.id}`}>
                        <Text strong className="clickable large_text">{movie.title}</Text>
                    </Link>
                    {
                        removeFromPlaylist &&
                        <Button onClick={removeFromPlaylist} icon={<CloseOutlined/>}
                                style={{marginTop: "0.6rem", minWidth: "2rem"}}/>
                    }
                </Flex>

                <Paragraph ellipsis={{rows: 3, expandable: true, symbol: 'more'}}>
                    {movie.overview}
                </Paragraph>
            </Flex>
        </Flex>
  );
}

export default MovieCard;