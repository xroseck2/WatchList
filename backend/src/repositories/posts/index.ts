import createPost from './create';
import {
  findPostByAuthorId, findPostByClubId, findPostById, getAllPosts,
} from './read';

const postRepository = {
  createPost,
  findPostById,
  findPostByAuthorId,
  findPostByClubId,
  getAllPosts,
};

export default postRepository;
