import { Request, Response } from 'express';
import { handleRepositoryErrors, parseRequest } from '../../utils';
import repository from '../../repositories/playlists/index';
import {
  queryPaginationRequestSchema,
  paramsIdRequestSchema,
} from '../generalValidationSchemas';
import {
  createPlaylistRequestSchema,
  updatePlaylistRequestSchema,
  updatePlaylistMoviesRequestSchema,
  searchPlaylistRequestSchema,
} from './validationSchemas';

const getPlaylists = async (req: Request, res: Response) => {
  const request = await parseRequest(queryPaginationRequestSchema, req, res);
  if (request === null) return;

  const result = await repository.getAllPlaylists(request.query);
  if (result.isErr) {
    handleRepositoryErrors(result.error, res);
    return;
  }

  res.send(result.value);
};

const createPlaylist = async (req: Request, res: Response) => {
  const request = await parseRequest(createPlaylistRequestSchema, req, res);
  if (request === null) return;

  let createdPlaylist;
  if (request.body.movieIds !== undefined) {
    // @ts-ignore
    createdPlaylist = await repository.createPlaylistWithMovies(request.body);
  } else {
    createdPlaylist = await repository.createPlaylist(request.body);
  }

  if (createdPlaylist.isErr) {
    handleRepositoryErrors(createdPlaylist.error, res);
    return;
  }

  res.status(201).send(createdPlaylist.value);
};

const deletePlaylist = async (req: Request, res: Response) => {
  const request = await parseRequest(paramsIdRequestSchema, req, res);
  if (request === null) return;

  const deletedPlaylist = await repository.deletePlaylist({ id: request.params.id });
  if (deletedPlaylist.isErr) {
    handleRepositoryErrors(deletedPlaylist.error, res);
    return;
  }

  res.send(deletedPlaylist.value);
};

const updatePlaylist = async (req: Request, res: Response) => {
  const request = await parseRequest(updatePlaylistRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedPlaylist = await repository.updatePlaylist(updateData);
  if (updatedPlaylist.isErr) {
    handleRepositoryErrors(updatedPlaylist.error, res);
    return;
  }

  res.send(updatedPlaylist.value);
};

const getPlaylist = async (req: Request, res: Response) => {
  const request = await parseRequest(searchPlaylistRequestSchema, req, res);
  if (request === null) return;

  const {
    id, name, userId, clubId,
  } = request.query;

  let playlist;
  if (id !== undefined) {
    playlist = await repository.findPlaylistById(id);
  } else if (name !== undefined) {
    playlist = await repository.findPlaylistByName(name);
  } else if (userId !== undefined) {
    playlist = await repository.findPlaylistByUser(userId);
  } else if (clubId !== undefined) {
    playlist = await repository.findPlaylistByClub(clubId);
  }

  if (playlist?.isErr) {
    handleRepositoryErrors(playlist.error, res);
    return;
  }

  res.send(playlist?.value);
};

const addMovies = async (req: Request, res: Response) => {
  const request = await parseRequest(updatePlaylistMoviesRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedPlaylist = await repository.addMoviesToPlaylist(updateData);
  if (updatedPlaylist.isErr) {
    handleRepositoryErrors(updatedPlaylist.error, res);
    return;
  }

  res.send(updatedPlaylist.value);
};

const removeMovies = async (req: Request, res: Response) => {
  const request = await parseRequest(updatePlaylistMoviesRequestSchema, req, res);
  if (request === null) return;

  const updateData = {
    id: request.params.id,
    ...request.body,
  };

  const updatedPlaylist = await repository.deleteMoviesFromPlaylist(updateData);
  if (updatedPlaylist.isErr) {
    handleRepositoryErrors(updatedPlaylist.error, res);
    return;
  }

  res.send(updatedPlaylist.value);
};

export const playlistsController = {
  getPlaylists,
  createPlaylist,
  deletePlaylist,
  updatePlaylist,
  getPlaylist,
  addMovies,
  removeMovies,
};
