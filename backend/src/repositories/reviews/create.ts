import { Result } from '@badrap/result';
import prisma from '../../client';
import { ReviewCreateData, ReviewReadResult } from './types/data';

const createReview = async (data: ReviewCreateData): ReviewReadResult => {
  try {
    const review = await prisma.review.create({
      data: {
        text: data.text,
        rating: data.rating,
        user: {
          connect: { id: data.userId },
        },
        movieId: data.movieId,
      },
    });
    return Result.ok(review);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export default createReview;
