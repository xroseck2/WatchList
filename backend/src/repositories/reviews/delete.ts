import { Result } from '@badrap/result';
import prisma from '../../client';
import { NotFoundError } from '../../utils';
import { ReviewIdData, ReviewReadResult } from './types/data';

export const deleteReview = async (data: ReviewIdData): ReviewReadResult => {
  try {
    const deleted = await prisma.review.delete({
      where: {
        id: data.id,
      },
    });
    if (!deleted) {
      return Result.err(new NotFoundError('No review found'));
    }
    return Result.ok(deleted);
  } catch (error) {
    return Result.err(error as Error);
  }
};
