import { Router } from 'express';
import { reviewsController } from './controller';

/**
 * @swagger
 * tags:
 *   name: Reviews
 *   description: The reviews managing API
 * components:
 *   schemas:
 *     Review:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the review
 *         rating:
 *           type: number
 *           format: float
 *           description: The rating of the review
 *         text:
 *           type: string
 *           description: The text of the review
 *         movieId:
 *           type: string
 *           description: The ID of movie that the review is for
 *         user:
 *           $ref: '#/components/schemas/User'
 *           description: The user who wrote the review
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The date the review was added
 *     CreateReviewRequest:
 *       type: object
 *       required:
 *         - text
 *         - rating
 *         - userId
 *         - movieId
 *       properties:
 *         text:
 *           type: string
 *           minLength: 1
 *           description: The text of the review
 *         rating:
 *           type: number
 *           minimum: 1
 *           maximum: 10
 *           description: The rating of the review
 *         userId:
 *           type: string
 *           description: The ID of the user
 *         movieId:
 *           type: string
 *           description: The ID of movie that the review is for
 *     UpdateReviewRequest:
 *       type: object
 *       properties:
 *         text:
 *           type: string
 *           minLength: 1
 *           description: The text of the review
 *         rating:
 *           type: number
 *           minimum: 1
 *           maximum: 10
 *           description: The rating of the review
 */
export const reviewsRouter = Router();

/**
 * @swagger
 * /api/v1/reviews:
 *   get:
 *     summary: Return page with reviews
 *     tags: [Reviews]
 *     parameters:
 *       - in: query
 *         name: size
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The number of reviews to return
 *           required: false
 *       - in: query
 *         name: page
 *         schema:
 *           type: number
 *           minimum: 1
 *           description: The page number to return
 *           required: false
 *     responses:
 *       200:
 *         description: A list of reviews.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Review'
 *       500:
 *         description: Some server error
 */
reviewsRouter.get('/', reviewsController.getReviews);

/**
 * @swagger
 * /api/v1/reviews/search:
 *   get:
 *     summary: Search for a review by ID, user ID, or movie ID
 *     tags: [Reviews]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the review to search
 *         required: false
 *       - in: query
 *         name: userId
 *         schema:
 *           type: string
 *           description: The user ID of the review to search
 *         required: false
 *       - in: query
 *         name: movieId
 *         schema:
 *           type: string
 *           description: The movie ID of the review to search
 *         required: false
 *     responses:
 *       200:
 *         description: The review was successfully found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Review'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID, user ID, or movie ID did not match any existing review
 *       500:
 *         description: Some server error
 */
reviewsRouter.get('/search', reviewsController.getReview);

/**
 * @swagger
 * /api/v1/reviews:
 *   post:
 *     summary: Create a new review
 *     tags: [Reviews]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateReviewRequest'
 *     responses:
 *       201:
 *         description: The review was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Review'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       500:
 *         description: Some server error
 */
reviewsRouter.post('/', reviewsController.createReview);

/**
 * @swagger
 * /api/v1/reviews/{id}:
 *   delete:
 *     summary: Delete a review
 *     tags: [Reviews]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the review to delete
 *           required: true
 *     responses:
 *       200:
 *         description: The review was successfully deleted
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Review'
 *       404:
 *         description: Bad request, the provided ID did not match any existing review
 *       500:
 *         description: Some server error
 */
reviewsRouter.delete('/:id', reviewsController.deleteReview);

/**
 * @swagger
 * /api/v1/reviews/{id}:
 *   put:
 *     summary: Update a review
 *     tags: [Reviews]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           description: The ID of the review to update
 *           required: true
 *     requestBody:
 *       required: true
 *       content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UpdateReviewRequest'
 *     responses:
 *       200:
 *         description: The review was successfully updated
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Review'
 *       400:
 *         description: Bad request, the provided data did not match the required schema
 *       404:
 *         description: Bad request, the provided ID did not match any existing review
 *       500:
 *         description: Some server error
 */
reviewsRouter.put('/:id', reviewsController.updateReview);
