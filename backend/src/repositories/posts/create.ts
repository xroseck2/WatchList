import { Result } from '@badrap/result';
import prisma from '../../client';
import { PostCreateData, PostReadResult } from './types/data';

const createPost = async (data: PostCreateData): PostReadResult => {
  try {
    const post = await prisma.post.create({
      data: {
        message: data.message,
        author: {
          connect: { id: data.authorId },
        },
        club: {
          connect: { id: data.clubId },
        },
      },
    });
    return Result.ok(post);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export default createPost;
