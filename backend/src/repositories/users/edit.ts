import { Prisma } from '@prisma/client';
import { Result } from '@badrap/result';
import {
  UserClubData, UserEditData, UserPlaylistData, UserReadResult,
} from './types/data';
import prisma from '../../client';
import { ConflictError, NotFoundError } from '../../utils';

export const updateUser = async (data: UserEditData): UserReadResult => {
  const updateData: Prisma.UserUpdateInput = {
    ...(data.name && { name: data.name }),
    ...(data.email && { email: data.email }),
    ...(data.password && { password: data.password }),
  };

  try {
    const result = await prisma.user.update({
      where: {
        id: data.id,
      },
      data: updateData,
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
      },
    });
    if (!result) {
      return Result.err(new NotFoundError('No users found'));
    }
    return Result.ok(result);
  } catch (error) {
    return Result.err(error as Error);
  }
};

export const addPlaylistsToUser = async (data: UserPlaylistData): UserReadResult => {
  try {
    const updatedUser = await prisma.user.update({
      where: {
        id: data.id,
      },
      data: {
        playlists: {
          connect: data.playlistIds.map((playlistId) => ({ id: playlistId })),
        },
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
        playlists: true,
      },
    });

    return Result.ok(updatedUser);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`user repository addPlaylistToUser error${error.message}`));
    }
    return Result.err(error as Error);
  }
};

export const deletePlaylistsFromUser = async (data: UserPlaylistData): UserReadResult => {
  try {
    const updatedUser = await prisma.user.update({
      where: {
        id: data.id,
      },
      data: {
        playlists: {
          disconnect: data.playlistIds.map((playlistId) => ({ id: playlistId })),
        },
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
        playlists: true,
      },
    });

    return Result.ok(updatedUser);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`user repository deletePlaylistFromUser error${error.message}`));
    }
    return Result.err(error as Error);
  }
};

export const userJoinClub = async (data: UserClubData): UserReadResult => {
  try {
    const updatedUser = await prisma.user.update({
      where: {
        id: data.id,
      },
      data: {
        clubs: {
          connect: {
            id: data.clubId,
          },
        },
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
        clubs: true,
      },
    });
    return Result.ok(updatedUser);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`user repository deletePlaylistFromUser error${error.message}`));
    }
    return Result.err(error as Error);
  }
};

export const userLeaveClub = async (data: UserClubData): UserReadResult => {
  try {
    const updatedUser = await prisma.user.update({
      where: {
        id: data.id,
      },
      data: {
        clubs: {
          disconnect: {
            id: data.clubId,
          },
        },
      },
      select: {
        id: true,
        email: true,
        name: true,
        createdAt: true,
        clubs: true,
      },
    });
    return Result.ok(updatedUser);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      console.log('error code:', error.code, error.message);
      return Result.err(new ConflictError(`user repository deletePlaylistFromUser error${error.message}`));
    }
    return Result.err(error as Error);
  }
};
