import { z } from 'zod';
import { paramsIdRequestSchema } from '../generalValidationSchemas';

export const createUserRequestSchema = z.object({
  body: z.object({
    name: z.string().min(1).max(255),
    email: z.string().email(),
    password: z.string().min(10).max(255),
  }),
});

export const updateUserRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      name: z.string().min(1).max(255).optional(),
      email: z.string().email().optional(),
      password: z.string().min(10).max(255).optional(),
    }),
  }),
);

export const updateUserPlaylistsRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      playlistIds: z.array(z.string().cuid()),
    }),
  }),
);

export const updateUserClubsRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      clubId: z.string().cuid(),
    }),
  }),
);

export const searchUserRequestSchema = z.object({
  query: z.object({
    id: z.string().cuid().optional(),
    name: z.string().min(1).max(255).optional(),
    email: z.string().email().optional(),
  }),
}).refine((data) => {
  const definedProps = Object.values(data.query).filter((prop) => prop !== undefined);
  return definedProps.length >= 1;
}, {
  message: "Either 'id', 'name' or 'email' must be provided",
});

export const verifyUserRequestSchema = z.object({
  body: z.object({
    id: z.string().cuid().optional(),
    name: z.string().min(1).max(255).optional(),
    email: z.string().email().optional(),
    password: z.string().min(10).max(255),
  }),
}).refine((data) => {
  const definedProps = Object.values(data.body).filter((prop) => prop !== undefined);
  return definedProps.length >= 2;
}, {
  message: "Either 'id', 'name' or 'email' must be provided together with password",
});
