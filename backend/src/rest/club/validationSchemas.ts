import { z } from 'zod';
import { paramsIdRequestSchema } from '../generalValidationSchemas';

export const createClubRequestSchema = z.object({
  body: z.object({
    name: z.string().min(1).max(255),
    ownerId: z.string().cuid(),
  }),
});

export const updateClubRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      name: z.string().min(1).max(255).optional(),
      ownerId: z.string().cuid().optional(),
    }),
  }),
);

export const updateClubPlaylistsRequestSchema = paramsIdRequestSchema.and(
  z.object({
    body: z.object({
      playlistIds: z.array(z.string().cuid()),
    }),
  }),
);

export const searchClubRequestSchema = z.object({
  query: z.object({
    id: z.string().cuid().optional(),
    name: z.string().min(1).max(255).optional(),
  }),
}).refine((data) => {
  const definedProps = Object.values(data.query).filter((prop) => prop !== undefined);
  return definedProps.length >= 1;
}, {
  message: "Either 'id' or 'name' must be provided",
});
