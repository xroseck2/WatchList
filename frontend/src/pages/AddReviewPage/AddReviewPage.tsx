import {Button, Flex, Input, Modal, Rate, Row, Typography} from "antd";
import {SetStateAction, useContext, useState} from "react";
import "./add_review_page.css";
import Search from "antd/es/input/Search";
import ModalMovieRow from "../../components/ModalMovieRow/ModalMovieRow.tsx";
import useWindowDimension from "../../hooks/useWindowDimension.tsx";
import movieApi from "../../api/movieApi.ts";
import {MovieDetailed} from "../../models/movie.ts";
import reviewApi from "../../api/reviewApi.ts";
import {CreateReview} from "../../models/review.ts";
import {UserContext, UserContextType} from "../../contexts/UserContext.tsx";
import {useNavigate} from "react-router-dom";

const {Text} = Typography;
const {TextArea} = Input;
type Movie = {
  id: number;
  title: string;
  year: string;
}

export default function AddReviewPage() {
  const {width} = useWindowDimension();
  const {user} = useContext<UserContextType>(UserContext);

  const [movie, setMovie] = useState<Movie | null>(null);
  const [rating, setRating] = useState<number>(0);
  const [isOpen, setIsOpen] = useState(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [foundMovies, setFoundMovies] = useState<MovieDetailed[]>([]);
  const [textAreaValue, setTextAreaValue] = useState("");
  const navigate = useNavigate();
  const handleTextAreaChange = (event: { target: { value: SetStateAction<string>; }; }) => {
      setTextAreaValue(event.target.value);
    };

    const handleCancel = () => {
        setIsOpen(false);
    };

    const onSearch = (value: string) => {
        if(value.length == 0) {
            return
        }
        setIsOpen(true);
        setLoading(true);

        movieApi.getAllByName(value, 1).then((res) => {
            console.log(res)
            setFoundMovies(res.results);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }

    const onSubmit = () => {
        const review: CreateReview = {
          userId: user!.id,
          movieId: movie!.id.toString(),
          rating: rating,
          text: textAreaValue
        }
        reviewApi.createSingle(review).then(() => {
          Modal.success({
            content: `Review was successfully submitted!`,
          })
          navigate("/user/" + user?.id)
        }).catch(() => {
          Modal.error({
            content: "Review was not submitted!",
          })
        });
    }
    return (
        <Flex vertical align="center" justify="space-around" className="add_page box">
            <Text className="add__heading">
                Creating a review
            </Text>

            <Row align="middle">
                <Text className="add__label">
                    Find movie:
                </Text>
                <Search placeholder="Movie name" className="add__search"
                    size="large" onSearch={onSearch}/>
            </Row>

            <Flex vertical align="center">
                {movie &&
                        <Text className="add__title">{movie.title} ({movie.year})</Text>
                }
                <Flex className="box" align="center" vertical={width < 600} style={{width: "100%"}}>
                    <Rate tooltips={["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]}
                          count={10} defaultValue={rating} onChange={(value) => setRating(value)}
                          style={{fontSize: "2rem"}}
                    />
                    <Text className="rating__text">{rating}</Text>
                </Flex>
            </Flex>

            <Row>
                <TextArea className="add__review" rows={6}
                          onChange={handleTextAreaChange}
                          placeholder="Write your review here"/>
            </Row>

            <Button onClick={onSubmit}>
                Submit review
            </Button>

            <Modal title="Select movie" open={isOpen} onCancel={handleCancel}
                   footer={[]} loading={loading} width="fit-content"
                   className="add__modal"
            >
                {foundMovies.map((v) =>
                    <ModalMovieRow key={v.id} movieId={v.id} title={v.title} year={v.release_date.substring(0, 4)}
                                   setMovie={setMovie} closeDialog={handleCancel}/>
                )}
            </Modal>

        </Flex>
    );
}